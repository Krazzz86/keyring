# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
update_fastlane

default_platform(:android)

platform :android do

  desc "Upload a new version to the Google Play"
  lane :upload do |options|
    upload_to_play_store(
      track: options[:track],
      aab: '../build/app/outputs/bundle/release/app-release.aab',
      json_key_data: ENV['google_play_service_account_api_key_json'],
      mapping_paths: [
        '../build/app/outputs/mapping/release/mapping.txt',
        '../build/app/outputs/native-debug-symbols.zip',
      ]
    )
  end

  lane :changelog do |options|
    changelog_from_git_commits
    path = "./metadata/android/en-GB/changelogs/"
    FileUtils.mkdir_p(path)
    path += options[:version_code] + ".txt"
    sh("touch", path)
    File.write(path, lane_context[SharedValues::FL_CHANGELOG][0,500])
  end

  lane :version_code do |options|
    google_play_track_version_codes(
      json_key_data: ENV['google_play_service_account_api_key_json'],
      track: options[:track],
    )
  end

  lane :promote do |options|
    upload_to_play_store(
      track: options[:track],
      track_promote_to: options[:track_promote_to],
      version_code: options[:version_code],
      rollout: "1",
      json_key_data: ENV['google_play_service_account_api_key_json'],
      skip_upload_apk: true,
      skip_upload_metadata: true,
      skip_upload_images: true,
      skip_upload_screenshots: true,
    )
  end

end
