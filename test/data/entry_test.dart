import 'package:flutter_test/flutter_test.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_type.dart';
import 'package:keyring/data/hash_algorithm.dart';

void main() {
  group('Entry', () {
    final entry = Entry(
        secret: 'SECRET',
        algorithm: HashAlgorithm.sha256,
        digits: 6,
        label: 'label',
        counter: 0,
        period: 30,
        type: EntryType.totp,
        id: 0,
        issuer: 'issuer');

    test('Constructor', () {
      expect(entry.id, 0);
      expect(entry.secret, 'SECRET');
      expect(entry.label, 'label');
      expect(entry.issuer, 'issuer');
      expect(entry.counter, 0);
      expect(entry.period, 30);
      expect(entry.digits, 6);
      expect(entry.algorithm, HashAlgorithm.sha256);
      expect(entry.type, EntryType.totp);
    });

    test('URI', () {
      final uri = entry.toURI();
      final newEntry = Entry.tryFromURI(uri);

      expect(newEntry, isNotNull);

      expect(entry.secret, newEntry!.secret);
      expect(entry.label, newEntry.label);
      expect(entry.issuer, newEntry.issuer);
      expect(entry.counter, newEntry.counter);
      expect(entry.period, newEntry.period);
      expect(entry.digits, newEntry.digits);
      expect(entry.algorithm, newEntry.algorithm);
      expect(entry.type, newEntry.type);
    });

    test('URI', () {
      entry.type = EntryType.hotp;
      final token = entry.calculateToken();
      expect(token, '356306');
    });
  });
}
