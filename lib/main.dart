import 'dart:async';
import 'dart:io';

import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'my_app.dart';

void main() async {
  runZonedGuarded<Future<void>>(() async {
    WidgetsFlutterBinding.ensureInitialized();

    if (isMobile()) {
      await Firebase.initializeApp();
    }

    if (kDebugMode && isMobile()) {
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
    }

    if (isMobile()) {
      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    }

    if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
      // Initialize FFI
      sqfliteFfiInit();
      // Change the default factory
      databaseFactory = databaseFactoryFfi;
    }

    runApp(const MyApp());

    if (!kIsWeb && !isMobile() && Platform.isLinux) {
      doWhenWindowReady(() {
        const initialSize = Size(1280, 720);
        appWindow.minSize = const Size(600, 600);
        appWindow.size = initialSize;
        appWindow.alignment = Alignment.center;
        appWindow.title = 'Keyring';
        appWindow.show();
      });
    }
  }, (error, stack) {
    if (isMobile()) {
      FirebaseCrashlytics.instance.recordError(error, stack);
    } else {
      // ignore: avoid_print
      print('Error: $error, $stack');
    }
  });
}
