import 'package:flutter/foundation.dart';
import 'package:keyring/data/abstract_database_helper.dart';
import 'package:keyring/data/database_helper.dart';
import 'package:keyring/data/web_database_helper.dart';
import 'package:keyring/utils/is_mobile.dart';

import 'entry.dart';

class EntryModel extends ChangeNotifier {
  EntryModel() {
    getEntries();
  }

  AbstractDatabaseHelper databaseHelper = !isMobile() ? WebDatabaseHelper() : DatabaseHelper();
  List<Entry>? _entries;
  List<Entry>? get entries => _entries;

  Future<List<Entry>?> getEntries() async {
    _entries = await databaseHelper.getEntries();
    notifyListeners();
    return entries;
  }

  Future<Entry> insert(Entry entry) async {
    Entry output = await databaseHelper.insert(entry);
    getEntries();
    return output;
  }

  Future<int> edit(Entry entry) async {
    int output = await databaseHelper.update(entry);
    getEntries();
    return output;
  }

  Future<int> remove(Entry entry) async {
    if (entry.id != null) {
      int output = await databaseHelper.delete(entry.id!);
      getEntries();
      return output;
    } else {
      return 0;
    }
  }
}
