// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:flutter/painting.dart';
import 'package:keyring/data/entry_type.dart';
import 'package:keyring/data/hash_algorithm.dart';
import 'package:otp/otp.dart';
import 'package:uri/uri.dart';

const String columnId = '_id';
const String columnLabel = 'label';
const String columnSecret = 'secret';
const String columnPeriod = 'period';
const String columnDigits = 'digits';
const String columnIssuer = 'issuer';
const String columnCounter = 'counter';
const String columnType = 'type';
const String columnHashAlgorythm = 'algorithm';

class Entry {
  Entry({
    required this.label,
    required this.secret,
    required this.period,
    required this.digits,
    required this.algorithm,
    required this.type,
    required this.counter,
    this.id,
    this.issuer,
  });

  Entry.fromMap(Map<dynamic, dynamic> map) {
    id = map[columnId] ?? -1;
    label = map[columnLabel] as String;
    secret = (map[columnSecret] as String).toUpperCase();
    period = map[columnPeriod] as int? ?? 30;
    digits = map[columnDigits] as int;
    issuer = map[columnIssuer] as String?;
    counter = map[columnCounter] as int? ?? 0;
    type = map[columnType] == 'TOTP' ? EntryType.totp : EntryType.hotp;
    algorithm = map[columnHashAlgorythm] == 'SHA1'
        ? HashAlgorithm.sha1
        : map[columnHashAlgorythm] == 'SHA256'
            ? HashAlgorithm.sha256
            : HashAlgorithm.sha512;
  }

  factory Entry.fromDynamic({dynamic value}) {
    return Entry(
      id: value.id as int?,
      label: value.label as String,
      secret: value.secret as String,
      period: value.period as int,
      digits: value.digits as int,
      issuer: value.issuer as String?,
      algorithm: value.algorithm as HashAlgorithm,
      type: value.type as EntryType,
      counter: value.counter as int,
    );
  }

  late int? id;
  late String label;
  late String secret;
  late String? issuer;
  late int counter;
  late int period;
  late int digits;
  late HashAlgorithm algorithm;
  late EntryType type;

  Map<String, Object?> toMap() {
    var map = <String, Object?>{
      columnLabel: label,
      columnSecret: secret.toUpperCase(),
      columnPeriod: period,
      columnDigits: digits,
      columnHashAlgorythm: _algorythmToString(),
      columnIssuer: issuer,
      columnType: _typeToString(),
      columnCounter: counter,
    };
    map[columnId] = id;
    return map;
  }

  Map<String, dynamic> toJson() => toMap();

  @override
  String toString() {
    return '''{
  "id": $id,
  "$columnLabel": "$label",
  "$columnSecret": "$secret",
  "$columnPeriod": $period,
  "$columnDigits": $digits,
  "issuer": "$issuer",
  "counter": $counter,
  "algorithm": "${_algorythmToString()}",
  "type": "${_typeToString()}",
}
''';
  }

  String _algorythmToString() {
    return algorithm
        .toString()
        .replaceFirst('HashAlgorithm.', '')
        .toUpperCase();
  }

  String _typeToString() {
    return type.toString().replaceFirst('EntryType.', '').toUpperCase();
  }

  static Entry? tryFromDynamic({dynamic value}) {
    try {
      return Entry.fromDynamic(value: value);
    } catch (exception) {
      return null;
    }
  }

  static Entry? tryFromMap({required Map<String, dynamic> value}) {
    try {
      return Entry.fromMap(value);
    } catch (exception) {
      return null;
    }
  }

  static Entry? tryFromURI(String input) {
    try {
      final Uri uri = Uri.dataFromString(input);
      final UriTemplate issuerTemplate = UriTemplate(
          'otpauth://{type}/{issuer}:{label}{?secret,algorithm,digits,period}');
      final UriTemplate labelTemplate = UriTemplate(
          'otpauth://{type}/{label}{?secret,algorithm,digits,period}');
      Map<String, String> parsedUri;
      try {
        final UriParser parser = UriParser(issuerTemplate);
        parsedUri = parser.parse(uri);
      } catch (e) {
        try {
          final UriParser parser = UriParser(labelTemplate);
          parsedUri = parser.parse(uri);
        } catch (e) {
          return null;
        }
      }
      final dynamic jsonUri = {};
      final List<String> intValues = <String>['digits', 'period', 'counter'];

      parsedUri.forEach((String key, String value) {
        if (key == 'type' || key == 'algorithm') {
          jsonUri[key] = value.toUpperCase();
          return;
        }
        if (intValues.contains(key)) {
          jsonUri[key] = int.tryParse(value);
          return;
        }
        jsonUri[key] = value;
      });
      return Entry.fromMap(jsonUri);
    } catch (e) {
      return null;
    }
  }

  String toURI() {
    return 'otpauth://${_typeToString().toLowerCase()}/${issuer != null && issuer!.isNotEmpty ? issuer! + ':' : ''}$label?secret=${secret.toLowerCase()}&algorithm=${algorithm.toString().replaceFirst('HashAlgorithm.', '').toUpperCase()}&digits=$digits&period=$period&counter=$counter';
  }

  Algorithm _switchAlgorithm(HashAlgorithm algorithm) {
    switch (algorithm) {
      case HashAlgorithm.sha1:
        return Algorithm.SHA1;
      case HashAlgorithm.sha256:
        return Algorithm.SHA256;
      case HashAlgorithm.sha512:
        return Algorithm.SHA512;
      default:
        return Algorithm.SHA1;
    }
  }

  String calculateToken() {
    final int now = DateTime.now().millisecondsSinceEpoch;
    try {
      if (type == EntryType.totp) {
        String output = OTP.generateTOTPCodeString(
          secret,
          now,
          length: digits,
          interval: period,
          algorithm: _switchAlgorithm(algorithm),
          isGoogle: true,
        );
        return output;
      } else {
        String output = OTP.generateHOTPCodeString(
          secret,
          counter,
          length: digits,
          algorithm: _switchAlgorithm(algorithm),
        );
        return output;
      }
    } catch (e) {
      return '';
    }
  }

  @override
  int get hashCode => hashValues(
        id,
        label,
        secret,
        issuer,
        counter,
        period,
        digits,
        algorithm,
        type,
      );

  @override
  bool operator ==(Object other) {
    return other is Entry &&
        id == other.id &&
        label == other.label &&
        secret == other.secret &&
        issuer == other.issuer &&
        counter == other.counter &&
        period == other.period &&
        digits == other.digits &&
        algorithm == other.algorithm &&
        type == other.type;
  }
}
