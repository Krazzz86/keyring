import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:keyring/data/abstract_database_helper.dart';
import 'package:keyring/data/entry.dart';
import 'package:sqflite/sqflite.dart';

const String tableName = 'entry';
const String path = 'entres.db';

class DatabaseHelper implements AbstractDatabaseHelper {
  Database? _db;
  Future<Database> get _database async {
    if (_db == null) {
      await _open();
    }
    return _db!;
  }

  final _secureStorage = const FlutterSecureStorage();

  Future _open() async {
    String databasesPath = await getDatabasesPath();
    _db = await openDatabase(
      databasesPath + path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute(
          '''
            create table $tableName (
              $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
              $columnLabel TEXT NOT NULL,
              $columnPeriod INTEGER,
              $columnDigits INTEGER NOT NULL,
              $columnHashAlgorythm TEXT NOT NULL,
              $columnType TEXT NOT NULL,
              $columnCounter INTEGER,
              $columnIssuer TEXT
            )
          ''',
        );
      },
    );
  }

  @override
  Future<Entry> insert(Entry entry) async {
    Map<String, Object?> entryToInsert = entry.toMap();
    entryToInsert.remove(columnId);
    entryToInsert.remove(columnSecret);
    entry.id = await (await _database).insert(
      tableName,
      entryToInsert,
    );
    await _secureStorage.write(key: entry.id.toString(), value: entry.secret);
    return entry;
  }

  @override
  Future<Entry?> getEntry(int id) async {
    List<Map> maps = await (await _database).query(
      tableName,
      columns: [
        columnId,
        columnLabel,
        columnPeriod,
        columnDigits,
        columnHashAlgorythm,
        columnIssuer,
        columnCounter,
        columnType,
      ],
      where: '$columnId = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      String? secret = await _secureStorage.read(key: id.toString());
      if (secret != null) {
        dynamic newMap = Map.of(maps.first);
        newMap[columnSecret] = secret;
        return Entry.fromMap(newMap);
      }
    }
    return null;
  }

  @override
  Future<List<Entry>> getEntries() async {
    List<Map> maps = await (await _database).query(
      tableName,
      columns: [
        columnId,
        columnLabel,
        columnPeriod,
        columnDigits,
        columnHashAlgorythm,
        columnIssuer,
        columnCounter,
        columnType,
      ],
    );
    if (maps.isNotEmpty) {
      Map<String, String> secrets = await _secureStorage.readAll();
      return maps.map((e) {
        dynamic newMap = Map.of(e);
        newMap[columnSecret] = secrets[e[columnId].toString()];
        return Entry.fromMap(newMap);
      }).toList();
    }
    return [];
  }

  @override
  Future<int> delete(int id) async {
    int output = await (await _database)
        .delete(tableName, where: '$columnId = ?', whereArgs: [id]);
    await _secureStorage.delete(key: id.toString());
    return output;
  }

  @override
  Future<int> update(Entry entry) async {
    Map<String, Object?> entryToInsert = entry.toMap();
    entryToInsert.remove(columnSecret);
    int output = await (await _database).update(tableName, entryToInsert,
        where: '$columnId = ?', whereArgs: [entry.id]);
    await _secureStorage.write(key: entry.id.toString(), value: entry.secret);
    return output;
  }

  Future close() async => _db?.close();
}
