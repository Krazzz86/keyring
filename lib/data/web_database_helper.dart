import 'dart:convert';
import 'dart:math';

import 'package:keyring/data/abstract_database_helper.dart';
import 'package:keyring/data/entry.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WebDatabaseHelper implements AbstractDatabaseHelper {
  SharedPreferences? _sharedPreferences;
  Future<SharedPreferences> get sharedPreferences async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
    return _sharedPreferences!;
  }

  @override
  Future<int> delete(int id) async {
    bool success = await (await sharedPreferences).remove('ENTRY.$id');
    return success ? id : -1;
  }

  @override
  Future<List<Entry>> getEntries() async {
    final keys = (await sharedPreferences).getKeys();
    keys.retainWhere((key) => key.contains('ENTRY.'));

    final importedEntries = (await Future.wait(
      keys.map(
        (key) async => (await sharedPreferences).getString(key),
      ),
    ));

    List<String> savedEntries = [];
    for (var element in importedEntries) {
      if (element != null) {
        savedEntries.add(element);
      }
    }

    final parsedEntries =
        savedEntries.map((entry) => Entry.fromMap(jsonDecode(entry))).toList();

    return parsedEntries;
  }

  @override
  Future<Entry?> getEntry(int id) async {
    String? value = (await sharedPreferences).getString('ENTRY.$id');
    if (value != null) {
      return Entry.fromMap(jsonDecode(value));
    } else {
      return null;
    }
  }

  @override
  Future<Entry> insert(Entry entry) async {
    final entries = await getEntries();
    final id = entries.isEmpty
        ? 1
        : (await getEntries()).map<int>((e) => e.id!).reduce(max) + 1;
    entry.id = id;
    await (await sharedPreferences)
        .setString('ENTRY.$id', jsonEncode(entry.toJson()));
    return entry;
  }

  @override
  Future<int> update(Entry entry) async {
    final success = await (await sharedPreferences)
        .setString('ENTRY.${entry.id}', jsonEncode(entry.toJson()));
    return success ? 1 : 0;
  }
}
