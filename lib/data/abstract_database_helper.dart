import 'package:keyring/data/entry.dart';

abstract class AbstractDatabaseHelper {
  Future<Entry> insert(Entry entry);
  Future<Entry?> getEntry(int id);
  Future<List<Entry>> getEntries();
  Future<int> delete(int id);
  Future<int> update(Entry entry);
}
