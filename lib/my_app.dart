import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:keyring/pages/add/scan.dart';
import 'package:keyring/pages/onboarding/onboarding.dart';
import 'package:keyring/pages/settings/pages/backup.dart';
import 'package:keyring/pages/settings/pages/language.dart';
import 'package:keyring/pages/settings/pages/privacy.dart';
import 'package:keyring/pages/welcome/welcome.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:keyring/utils/route_observer.dart';
import 'package:provider/provider.dart';

import 'data/entry_model.dart';
import 'pages/add/add.dart';
import 'pages/edit/edit.dart';
import 'pages/home/home.dart';
import 'pages/settings/pages/about.dart';
import 'pages/settings/pages/appearance.dart';
import 'pages/settings/settings.dart';
import 'pages/share/share.dart';
import 'utils/shared_preferences_model.dart';
import 'utils/theme_preference.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  final lightTheme = const NeumorphicThemeData();
  final NeumorphicThemeData darkTheme =
      const NeumorphicThemeData.dark().copyWith(
    shadowDarkColor: NeumorphicColors.decorationMaxDarkColor,
    shadowDarkColorEmboss: NeumorphicColors.decorationMaxDarkColor,
    shadowLightColor: NeumorphicColors.embossMaxDarkColor,
    shadowLightColorEmboss: NeumorphicColors.embossMaxDarkColor,
  );

  _addFlags() async {
    if (isMobile() && Platform.isAndroid) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt >= 30) {
        await FlutterWindowManager.addFlags(
            FlutterWindowManager.FLAG_LAYOUT_NO_LIMITS);
      }
    }
  }

  @override
  void initState() {
    _addFlags();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => EntryModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => SharedPreferencesModel(),
        ),
      ],
      child: Consumer(
        builder: (context, SharedPreferencesModel sharedPreferences, child) {
          return NeumorphicApp(
            onGenerateTitle: (BuildContext context) =>
                AppLocalizations.of(context)!.appName,
            themeMode: sharedPreferences.theme == ThemePreference.system
                ? ThemeMode.system
                : sharedPreferences.theme == ThemePreference.light
                    ? ThemeMode.light
                    : ThemeMode.dark,
            initialRoute: WelcomePage.routeName,
            theme: lightTheme,
            darkTheme: darkTheme,
            routes: <String, WidgetBuilder>{
              HomePage.routeName: (BuildContext context) => const HomePage(),
              SettingsPage.routeName: (BuildContext context) =>
                  const SettingsPage(),
              AddPage.routeName: (BuildContext context) => const AddPage(),
              EditPage.routeName: (BuildContext context) => const EditPage(),
              SharePage.routeName: (BuildContext context) => const SharePage(),
              OnboardingPage.routeName: (BuildContext context) =>
                  const OnboardingPage(),
              WelcomePage.routeName: (BuildContext context) =>
                  const WelcomePage(),
              LanguagePage.routeName: (BuildContext context) =>
                  const LanguagePage(),
              AboutPage.routeName: (BuildContext context) => const AboutPage(),
              AppearancePage.routeName: (BuildContext context) =>
                  const AppearancePage(),
              PrivacyPage.routeName: (BuildContext context) =>
                  const PrivacyPage(),
              BackupPage.routeName: (BuildContext context) =>
                  const BackupPage(),
              ScanPage.routeName: (BuildContext context) => const ScanPage(),
            },
            navigatorObservers: [
              if (isMobile()) ...[
                FirebaseAnalyticsObserver(analytics: analytics),
              ],
              routeObserver
            ],
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            locale: sharedPreferences.locale,
          );
        },
      ),
    );
  }
}
