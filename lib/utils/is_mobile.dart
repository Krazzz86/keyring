import 'dart:io';
import 'package:flutter/foundation.dart' show kIsWeb;

bool isMobile() {
  if (kIsWeb) {
    return false;
  } else {
    return Platform.isAndroid || Platform.isIOS;
  }
}
