import 'dart:convert';
import 'dart:typed_data';

import 'package:file_saver/file_saver.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:intl/intl.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:provider/provider.dart';

exportBackup(BuildContext context) async {
  AppLocalizations? localizations = AppLocalizations.of(context);
  EntryModel model = Provider.of<EntryModel>(context, listen: false);
  if (model.entries == null) {
    return;
  }
  final now = DateTime.now().toUtc();
  final DateFormat formatter = DateFormat('yyyy-MM-ddTH:mm:ss');
  final String formatted = formatter.format(now);
  final output = jsonEncode(model.entries);

  try {
    String result = await FileSaver.instance.saveFile(
      'keyring_backup_$formatted',
      Uint8List.fromList(utf8.encode(output)),
      'json',
      mimeType: MimeType.JSON,
    );
    ScaffoldMessenger.maybeOf(context)?.showSnackBar(
      SnackBar(
        content: SnackBarText(
          localizations?.componentBuckupExportSucess(
              model.entries!.length.toString(), result),
          textAlign: TextAlign.center,
        ),
      ),
    );
  } catch (exception, stack) {
    FirebaseCrashlytics.instance.recordError(exception, stack);
    ScaffoldMessenger.maybeOf(context)?.showSnackBar(
      SnackBar(
        content: SnackBarText(
          localizations?.componentBuckupExportError,
        ),
      ),
    );
  }
}
