import 'package:in_app_review/in_app_review.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:keyring/utils/shared_preferences_model.dart';

const week = Duration(
  days: 7,
);

/// Checks if we should show a rate request and shows it when appropriate
rate() async {
  if (!isMobile()) {
    return;
  }
  final InAppReview inAppReview = InAppReview.instance;
  final available = await inAppReview.isAvailable();

  if (!available) {
    return;
  }

  SharedPreferencesModel model =
      await SharedPreferencesModel().getPreferences();
  DateTime now = DateTime.now();

  _showRate() {
    inAppReview.requestReview();
    model.lastRateRequest = now;
  }

  if (model.lastRateRequest != null) {
    if (now.isAfter(
      model.lastRateRequest!.add(week),
    )) {
      _showRate();
    }
  } else if (model.firstRun != null) {
    if (now.isAfter(
      model.firstRun!.add(week),
    )) {
      _showRate();
    }
  }
}
