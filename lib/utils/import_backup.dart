import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:provider/provider.dart';

Future<List<Entry>?> importBackup(BuildContext context) async {
  EntryModel model = Provider.of<EntryModel>(context, listen: false);
  AppLocalizations? localizations = AppLocalizations.of(context);
  FilePickerResult? result = await FilePicker.platform.pickFiles();
  List<Entry>? output;

  void _onError(dynamic exception, StackTrace? stack) {
    if (!kIsWeb && exception != null && stack != null) {
      FirebaseCrashlytics.instance.recordError(exception, stack);
    }
    ScaffoldMessenger.maybeOf(context)?.showSnackBar(
      SnackBar(
        content: SnackBarText(
          localizations?.componentBuckupImportError,
        ),
      ),
    );
  }

  if (result != null) {
    try {
      String fileString = '';
      if (kIsWeb) {
        if (result.files.single.bytes != null) {
          fileString = utf8.decode(result.files.single.bytes!.toList());
        } else {
          return null;
        }
      } else {
        if (result.files.single.path != null) {
          final File file = File(result.files.single.path!);
          fileString = await file.readAsString();
        } else {
          return null;
        }
      }
      final List<dynamic> json = jsonDecode(fileString.trim());
      final List<Entry> entries = [];
      for (final entry in json) {
        Entry? newEntry = Entry.tryFromMap(value: entry);
        if (newEntry != null) {
          entries.add(newEntry);
        }
      }
      output = entries;
    } catch (exception, stack) {
      _onError(exception, stack);
      return null;
    }
    try {
      for (var entry in output) {
        await model.insert(entry);
      }

      ScaffoldMessenger.maybeOf(context)?.showSnackBar(
        SnackBar(
          content: SnackBarText(
            localizations?.componentBuckupImportSucess(
              output.length.toString(),
            ),
          ),
        ),
      );
    } catch (exception, stack) {
      _onError(exception, stack);
      return null;
    }
    return output;
  }
}
