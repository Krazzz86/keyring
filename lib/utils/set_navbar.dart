import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';

import 'is_mobile.dart';

void setNavbar(BuildContext context) {
  Brightness brightness = Theme.of(context).brightness;
  _setNavbarAndroid(brightness);
}

Future<void> _setNavbarAndroid(Brightness brightness) async {
  if (isMobile() && Platform.isAndroid) {
    bool isLight = brightness == Brightness.light;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    double opacity = 1;
    if (androidInfo.version.sdkInt >= 30) {
      await FlutterWindowManager.addFlags(
          FlutterWindowManager.FLAG_LAYOUT_NO_LIMITS);
      opacity = 0;
    }
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: isLight
            ? Color.fromRGBO(221, 230, 232, opacity)
            : Color.fromRGBO(45, 47, 47, opacity),
        systemNavigationBarContrastEnforced: false,
        systemNavigationBarIconBrightness:
            isLight ? Brightness.dark : Brightness.light,
        systemNavigationBarDividerColor: isLight
            ? Color.fromRGBO(221, 230, 232, opacity)
            : Color.fromRGBO(45, 47, 47, opacity),
        statusBarColor: isLight
            ? const Color.fromRGBO(221, 230, 232, 0)
            : const Color.fromRGBO(45, 47, 47, 0),
        statusBarIconBrightness: isLight ? Brightness.dark : Brightness.light,
      ),
    );
  }
}
