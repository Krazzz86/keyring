import 'package:keyring/utils/theme_preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences_model.dart';

class SharedPreferencesHelper {
  SharedPreferences? _sharedPreferences;
  Future<SharedPreferences> get sharedPreferences async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
    return _sharedPreferences!;
  }

  setTheme(ThemePreference value) async {
    (await sharedPreferences).setString(themeKey, value.toString());
  }

  Future<ThemePreference> getTheme() async {
    String input = (await sharedPreferences).getString(themeKey) ??
        'ThemePreference.system';
    ThemePreference output;
    switch (input) {
      case 'ThemePreference.system':
        output = ThemePreference.system;
        break;
      case 'ThemePreference.light':
        output = ThemePreference.light;
        break;
      case 'ThemePreference.dark':
        output = ThemePreference.dark;
        break;
      default:
        output = ThemePreference.system;
        break;
    }
    return output;
  }

  Future<String?> getString(String key) async {
    return (await sharedPreferences).getString(key);
  }

  Future<bool?> getBool(String key) async {
    try {
      return (await sharedPreferences).getBool(key);
    } catch (e) {
      return null;
    }
  }

  Future<bool?> setBool(String key, bool value) async {
    return (await sharedPreferences).setBool(key, value);
  }

  Future<int?> getInt(String key) async {
    try {
      return (await sharedPreferences).getInt(key);
    } catch (e) {
      return null;
    }
  }

  Future<bool> setInt(String key, int value) async {
    return (await sharedPreferences).setInt(key, value);
  }

  Future<bool> setString(String key, String? value) async {
    if (value == null) {
      return (await sharedPreferences).remove(key);
    } else {
      return (await sharedPreferences).setString(key, value);
    }
  }
}
