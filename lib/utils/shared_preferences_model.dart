import 'package:flutter/material.dart';
import 'package:keyring/services/analytics.dart';
import 'package:keyring/utils/theme_preference.dart';
import 'shared_preferences_helper.dart';

const onboardingSeenKey = 'ONBOARDING_SEEN';
const labelMaxNumberOfLinesKey = 'LABEL_MAX_NUMBER_OF_LINES';
const issuerMaxNumberOfLinesKey = 'ISSUER_MAX_NUMBER_OF_LINES';
const themeKey = 'THEME_PREFERENCE';
const localeKey = 'LOCALE_PREFERENCE';
const showGlobalProgressBarKey = 'SHOW_GLOBAL_PROGRESS_BAR';
const showIndividualProgressBarKey = 'SHOW_INDIVIDUAL_PROGRESS_BAR';
const globalProgressBarPeriodKey = 'GLOBAL_PROGRESS_BAR_PERIOD';
const individualProgressBarPeriodKey = 'INDIVIDUAL_PROGRESS_BAR_PERIOD';
const privateHomeScreenKey = 'PRIVATE_HOME_SCREEN';
const showIssuerKey = 'SHOW_ISSUER';
const firstRunKey = 'FIRST_RUN';
const lastRateRequestKey = 'LAST_RATE_REQUEST';

const onboardingSeenDefault = false;
const showGlobalProgressBarDefault = true;
const showIndividualProgressBarDefault = true;
const privateHomeScreenDefault = true;
const globalProgressBarPeriodDefault = 30;
const individualProgressBarPeriodDefault = 30;
const labelMaxNumberOfLinesDefault = 1;
const issuerMaxNumberOfLinesDefault = 1;
const showIssuerDefault = true;

class SharedPreferencesModel extends ChangeNotifier {
  SharedPreferencesModel() {
    _preferences = SharedPreferencesHelper();
    getPreferences();
  }
  SharedPreferencesHelper _preferences = SharedPreferencesHelper();
  ThemePreference _theme = ThemePreference.system;
  bool? _onboardingSeen;
  bool _showGlobalProgressBar = showGlobalProgressBarDefault;
  bool _showIndividualProgressBar = showIndividualProgressBarDefault;
  bool _privateHomeScreen = privateHomeScreenDefault;
  int _globalProgressBarPeriod = globalProgressBarPeriodDefault;
  int _individualProgressBarPeriod = individualProgressBarPeriodDefault;
  int _labelMaxNumberOfLines = labelMaxNumberOfLinesDefault;
  bool _showIssuer = showIssuerDefault;
  int _issuerMaxNumberOfLines = issuerMaxNumberOfLinesDefault;
  DateTime? _firstRun;
  DateTime? _lastRateRequest;
  Locale? _locale;

  ThemePreference get theme => _theme;
  bool? get onboardingSeen => _onboardingSeen;
  int get labelMaxNumberOfLines => _labelMaxNumberOfLines;
  bool get showIssuer => _showIssuer;
  int get issuerMaxNumberOfLines => _issuerMaxNumberOfLines;
  Locale? get locale => _locale;
  bool get showGlobalProgressBar => _showGlobalProgressBar;
  bool get showIndividualProgressBar => _showIndividualProgressBar;
  int get globalProgressBarPeriod => _globalProgressBarPeriod;
  int get individualProgressBarPeriod => _individualProgressBarPeriod;
  bool get privateHomeScreen => _privateHomeScreen;
  DateTime? get firstRun => _firstRun;
  DateTime? get lastRateRequest => _lastRateRequest;

  set theme(ThemePreference value) {
    if (_theme != value) {
      _theme = value;
      _preferences.setTheme(value);
      notifyListeners();
      AnalyticsService.setProperty(
        name: 'theme_preference',
        value: value.toString().replaceAll('ThemePreference.', ''),
      );
    }
  }

  setThemeWithoutSaving(ThemePreference value) {
    _theme = value;
    notifyListeners();
  }

  set onboardingSeen(bool? value) {
    if (_onboardingSeen != value) {
      _onboardingSeen = value;
      _preferences.setBool(onboardingSeenKey, value ?? false);
      notifyListeners();
    }
  }

  set labelMaxNumberOfLines(int value) {
    if (_labelMaxNumberOfLines != value) {
      _labelMaxNumberOfLines = value;
      _preferences.setInt(labelMaxNumberOfLinesKey, value);
      notifyListeners();
    }
  }

  set issuerMaxNumberOfLines(int value) {
    if (_issuerMaxNumberOfLines != value) {
      _issuerMaxNumberOfLines = value;
      _preferences.setInt(issuerMaxNumberOfLinesKey, value);
      notifyListeners();
    }
  }

  set showGlobalProgressBar(bool value) {
    if (_showGlobalProgressBar != value) {
      _showGlobalProgressBar = value;
      _preferences.setBool(showGlobalProgressBarKey, value);
      notifyListeners();
    }
  }

  set showIndividualProgressBar(bool value) {
    if (_showIndividualProgressBar != value) {
      _showIndividualProgressBar = value;
      _preferences.setBool(showIndividualProgressBarKey, value);
      notifyListeners();
    }
  }

  set globalProgressBarPeriod(int value) {
    if (_globalProgressBarPeriod != value) {
      _globalProgressBarPeriod = value;
      _preferences.setInt(globalProgressBarPeriodKey, value);
      notifyListeners();
    }
  }

  set individualProgressBarPeriod(int value) {
    if (_individualProgressBarPeriod != value) {
      _individualProgressBarPeriod = value;
      _preferences.setInt(individualProgressBarPeriodKey, value);
      notifyListeners();
    }
  }

  set privateHomeScreen(bool value) {
    if (_privateHomeScreen != value) {
      _privateHomeScreen = value;
      _preferences.setBool(privateHomeScreenKey, value);
      notifyListeners();
    }
  }

  set showIssuer(bool value) {
    if (_showIssuer != value) {
      _showIssuer = value;
      _preferences.setBool(showIssuerKey, value);
      notifyListeners();
    }
  }

  set locale(Locale? locale) {
    _locale = locale;
    _preferences.setString(localeKey, locale?.toLanguageTag());
    notifyListeners();
  }

  set firstRun(DateTime? firstRun) {
    if (firstRun != null) {
      _firstRun = firstRun;
      _preferences.setString(firstRunKey, firstRun.toIso8601String());
      notifyListeners();
    }
  }

  set lastRateRequest(DateTime? lastRateRequest) {
    if (lastRateRequest != null) {
      _lastRateRequest = lastRateRequest;
      _preferences.setString(
          lastRateRequestKey, lastRateRequest.toIso8601String());
      notifyListeners();
    }
  }

  Future<SharedPreferencesModel> getPreferences() async {
    List<dynamic> values = await Future.wait([
      _preferences.getTheme(),
      _preferences.getBool(onboardingSeenKey),
      _preferences.getInt(labelMaxNumberOfLinesKey),
      _preferences.getInt(issuerMaxNumberOfLinesKey),
      _preferences.getString(localeKey),
      _preferences.getBool(showGlobalProgressBarKey),
      _preferences.getInt(globalProgressBarPeriodKey),
      _preferences.getInt(privateHomeScreenKey),
      _preferences.getBool(showIndividualProgressBarKey),
      _preferences.getInt(individualProgressBarPeriodKey),
      _preferences.getBool(showIssuerKey),
      _preferences.getString(firstRunKey),
      _preferences.getString(lastRateRequestKey),
    ]);
    _theme = values[0] as ThemePreference;
    _onboardingSeen = values[1] as bool? ?? onboardingSeenDefault;
    _labelMaxNumberOfLines = values[2] ?? labelMaxNumberOfLinesDefault;
    _issuerMaxNumberOfLines = values[3] ?? issuerMaxNumberOfLinesDefault;
    try {
      _locale = values[4] != null ? Locale(values[4]) : null;
    } catch (e) {
      _locale = null;
    }
    _showGlobalProgressBar = values[5] ?? showGlobalProgressBarDefault;
    _globalProgressBarPeriod = values[6] ?? globalProgressBarPeriodDefault;
    _privateHomeScreen = values[7] ?? privateHomeScreenDefault;
    _showIndividualProgressBar = values[8] ?? showIndividualProgressBarDefault;
    _individualProgressBarPeriod =
        values[9] ?? individualProgressBarPeriodDefault;
    _showIssuer = values[10] ?? showIssuerDefault;
    if (values[11] == null) {
      _firstRun = DateTime.now();
      _preferences.setString(firstRunKey, _firstRun?.toIso8601String());
    } else {
      _firstRun = DateTime.tryParse(values[11]);
    }
    _lastRateRequest = DateTime.tryParse(values[12] ?? '');
    notifyListeners();
    AnalyticsService.setProperty(
      name: 'theme_preference',
      value: _theme.toString().replaceAll('ThemePreference.', ''),
    );
    return this;
  }

  @override
  String toString() {
    Map map = {
      onboardingSeenKey: _onboardingSeen,
      labelMaxNumberOfLinesKey: _labelMaxNumberOfLines,
      issuerMaxNumberOfLinesKey: _issuerMaxNumberOfLines,
      themeKey: _theme,
      localeKey: _locale,
      showGlobalProgressBarKey: _showGlobalProgressBar,
      showIndividualProgressBarKey: _showIndividualProgressBar,
      globalProgressBarPeriodKey: _globalProgressBarPeriod,
      individualProgressBarPeriodKey: _individualProgressBarPeriod,
      privateHomeScreenKey: _privateHomeScreen,
      showIssuerKey: _showIssuer,
      firstRunKey: _firstRun,
      lastRateRequestKey: _lastRateRequest
    };
    return map.toString();
  }
}
