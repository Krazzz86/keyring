import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/services/analytics.dart';

class MyNeumorphicIconButton extends StatelessWidget {
  const MyNeumorphicIconButton({
    required this.id,
    Key? key,
    this.icon,
    this.tooltip,
    this.onPressed,
    this.child,
  }) : super(key: key);
  final IconData? icon;
  final Widget? child;
  final Function()? onPressed;
  final String? tooltip;
  final String id;

  @override
  Widget build(BuildContext context) {
    if (child == null && icon == null) {
      throw ArgumentError.notNull('icon');
    }
    return ConstrainedBox(
      constraints: const BoxConstraints(minHeight: 56),
      child: NeumorphicButton(
        tooltip: tooltip,
        minDistance: -5,
        child: child ??
            Icon(
              icon,
              color: Theme.of(context).textTheme.headline3?.color,
            ),
        onPressed: () {
          AnalyticsService.logClick(id: id);
          onPressed?.call();
        },
        style: NeumorphicTheme.of(context)
            ?.value
            .current
            ?.appBarTheme
            .buttonStyle
            .copyWith(
              boxShape: const NeumorphicBoxShape.circle(),
              depth: 5.0,
            ),
      ),
    );
  }
}
