import 'dart:async';

import 'package:flutter/material.dart';

const int defaultDuration = 30;

class AppBarProgress extends StatefulWidget implements PreferredSizeWidget {
  const AppBarProgress({
    required this.stream,
    Key? key,
    this.period = defaultDuration,
  }) : super(key: key);
  final Stream<int> stream;
  final int? period;

  @override
  _AppBarProgressState createState() => _AppBarProgressState();

  @override
  Size get preferredSize => const Size.fromHeight(6.0);
}

class _AppBarProgressState extends State<AppBarProgress> {
  double value = 1;
  StreamSubscription? subscription;

  @override
  void initState() {
    super.initState();
    setState(() {
      subscription = widget.stream.listen((event) {
        int duration = (widget.period ?? defaultDuration) * 1000;
        final double remainder = event.remainder(duration).toDouble();
        setState(() {
          value = 1 - (remainder / duration);
        });
      });
    });
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: value,
      color: Theme.of(context).textTheme.headline3?.color,
      backgroundColor: Colors.black12,
    );
  }
}
