import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';

class WebBackButton extends StatelessWidget {
  const WebBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ModalRoute<dynamic>? parentRoute = ModalRoute.of(context);

    final bool useCloseButton =
        parentRoute is PageRoute<dynamic> && parentRoute.fullscreenDialog;

    return useCloseButton
        ? MyNeumorphicIconButton(
            id: 'web_close',
            icon: Icons.close_rounded,
            onPressed: () => Navigator.maybePop(context),
          )
        : MyNeumorphicIconButton(
            id: 'web_back',
            icon: Icons.arrow_back_rounded,
            onPressed: () => Navigator.maybePop(context),
          );
  }
}
