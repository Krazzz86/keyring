import 'package:flutter/material.dart';
import 'package:keyring/components/typography.dart';

class MyFormField extends StatelessWidget {
  const MyFormField({
    required this.child,
    this.label,
    Key? key,
    this.padding,
  }) : super(key: key);
  final Widget child;
  final String? label;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.only(top: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          label != null
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Headline4(
                    label,
                  ),
                )
              : Container(),
          child
        ],
      ),
    );
  }
}
