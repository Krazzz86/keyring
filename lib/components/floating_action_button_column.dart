import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:keyring/pages/add/add.dart';
import 'package:keyring/pages/add/scan.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:keyring/utils/route_observer.dart';
import 'package:provider/provider.dart';

class FloatingActionButtonColumn extends StatefulWidget {
  const FloatingActionButtonColumn({Key? key}) : super(key: key);

  @override
  _FloatingActionButtonColumnState createState() =>
      _FloatingActionButtonColumnState();
}

const Duration animationDuration = Duration(milliseconds: 250);

const double bottomPadding = 16;

class _FloatingActionButtonColumnState extends State<FloatingActionButtonColumn>
    with RouteAware {
  bool isOpen = false;

  _changeState() {
    setState(() {
      isOpen = !isOpen;
    });
  }

  _close() {
    setState(() {
      isOpen = false;
    });
  }

  @override
  void didChangeDependencies() {
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
    super.didChangeDependencies();
  }

  /// Called when the current route has been popped off.
  @override
  void didPop() {
    _close();
  }

  /// Called when a new route has been pushed, and the current route is no
  /// longer visible.
  @override
  void didPushNext() {
    _close();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return AnimatedContainer(
      constraints: BoxConstraints(
          minHeight: isOpen ? (72 * 3) + bottomPadding : 72 + bottomPadding),
      duration: animationDuration,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, bottomPadding),
        child: isMobile()
            ? Stack(
                alignment: AlignmentDirectional.bottomEnd,
                children: [
                  _AnimatedFAB(
                    position: 144,
                    show: isOpen,
                    child: Consumer(
                        builder: (context, EntryModel entryModel, child) {
                      return MyNeumorphicIconButton(
                          onPressed: () {
                            _changeState();
                            Navigator.of(context).pushNamed(ScanPage.routeName);
                          },
                          tooltip: localizations?.componentFABScanTooltip,
                          icon: Icons.camera_alt_rounded,
                          id: 'component_fab_scan');
                    }),
                  ),
                  _AnimatedFAB(
                    position: 72,
                    show: isOpen,
                    child: MyNeumorphicIconButton(
                        onPressed: () {
                          _changeState();
                          Navigator.of(context).pushNamed(AddPage.routeName);
                        },
                        tooltip: localizations?.componentFABEditTooltip,
                        icon: Icons.add_rounded,
                        id: 'component_fab_edit'),
                  ),
                  MyNeumorphicIconButton(
                    onPressed: () {
                      _changeState();
                    },
                    tooltip: isOpen
                        ? localizations?.componentFABCloseTooltip
                        : localizations?.componentFABAddTooltip,
                    child: AnimatedRotation(
                      duration: animationDuration,
                      turns: isOpen ? 0.125 : 0,
                      child: Icon(
                        Icons.add_rounded,
                        color: Theme.of(context).textTheme.headline3?.color,
                      ),
                    ),
                    id: isOpen ? 'component_fab_close' : 'component_fab_add',
                  )
                ],
              )
            : MyNeumorphicIconButton(
                onPressed: () {
                  _changeState();
                  Navigator.of(context).pushNamed(AddPage.routeName);
                },
                tooltip: localizations?.componentFABEditTooltip,
                icon: Icons.add_rounded,
                id: 'component_fab_edit',
              ),
      ),
    );
  }
}

class _AnimatedFAB extends StatelessWidget {
  const _AnimatedFAB({
    required this.child,
    required this.position,
    required this.show,
    Key? key,
  }) : super(key: key);
  final Widget child;
  final double position;
  final bool show;

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      duration: animationDuration,
      bottom: show ? position : 0,
      child: AnimatedOpacity(
        duration: animationDuration,
        opacity: show ? 1 : 0,
        child: child,
      ),
    );
  }
}
