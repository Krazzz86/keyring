import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';

class MyNeumorphicToggle extends StatefulWidget {
  const MyNeumorphicToggle({
    required this.children,
    required this.onChanged,
    Key? key,
    this.selectedIndex = 0,
  }) : super(key: key);
  final List<String> children;
  final int? selectedIndex;
  final void Function(int) onChanged;

  @override
  _MyNeumorphicToggleState createState() => _MyNeumorphicToggleState();
}

class _MyNeumorphicToggleState extends State<MyNeumorphicToggle> {
  @override
  void initState() {
    super.initState();
  }

  _foreground(String value) {
    return Center(
      child: Button(
        value,
      ),
    );
  }

  _background(String value) {
    return Opacity(
      opacity: 0.5,
      child: _foreground(value),
    );
  }

  ToggleElement _toggleElement(String value) {
    return ToggleElement(
      foreground: _foreground(value),
      background: _background(value),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NeumorphicToggle(
      style: NeumorphicToggleStyle(
        backgroundColor:
            NeumorphicTheme.of(context)?.current?.appBarTheme.buttonStyle.color,
        borderRadius: BorderRadius.circular(32),
      ),
      children: widget.children.map((e) => _toggleElement(e)).toList(),
      thumb: Neumorphic(),
      selectedIndex: widget.selectedIndex ?? 0,
      onChanged: widget.onChanged,
    );
  }
}
