import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/services/analytics.dart';

import 'my_neumorphic_icon_button.dart';

class RowWithButton extends StatefulWidget {
  const RowWithButton({
    required this.id,
    Key? key,
    this.label,
    this.tooltip,
    this.icon,
    this.child,
    this.onPressed,
    this.padding,
  }) : super(key: key);
  final String? label;
  final String? tooltip;
  final IconData? icon;
  final Widget? child;
  final Function()? onPressed;
  final EdgeInsetsGeometry? padding;
  final String id;

  @override
  _RowWithButtonState createState() => _RowWithButtonState();
}

class _RowWithButtonState extends State<RowWithButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      onTap: () {
        AnalyticsService.logClick(id: widget.id);
        widget.onPressed?.call();
      },
      child: Padding(
        padding: widget.padding ?? const EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Headline4(widget.label),
            MyNeumorphicIconButton(
              tooltip: widget.tooltip ?? widget.label,
              icon: widget.icon,
              child: widget.child,
              onPressed: () {
                widget.onPressed?.call();
              },
              id: widget.id,
            )
          ],
        ),
      ),
    );
  }
}
