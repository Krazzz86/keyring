import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';

class SwitchWithHeadline extends StatelessWidget {
  const SwitchWithHeadline({
    required this.onChanged,
    required this.value,
    Key? key,
    this.padding,
    this.label,
  }) : super(key: key);
  final EdgeInsets? padding;
  final String? label;
  final bool value;
  final Function(bool newValue) onChanged;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.only(top: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            flex: 1,
            child: Headline4(
              label,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
          ),
          NeumorphicSwitch(
            value: value,
            onChanged: onChanged,
          )
        ],
      ),
    );
  }
}
