import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_desktop_app_bar.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/components/web_back_button.dart';
import 'package:keyring/utils/is_mobile.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  const MyAppBar({
    required this.isScrolledToTop,
    this.leading,
    this.actions,
    this.title,
    this.bottom,
    Key? key,
  }) : super(key: key);
  final String? title;
  final Widget? leading;
  final List<Widget>? actions;
  final Widget? bottom;
  final bool isScrolledToTop;

  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize {
    if (title == null) {
      return const Size.fromHeight(0);
    } else {
      return const Size.fromHeight(NeumorphicAppBar.toolbarHeight);
    }
  }
}

class _MyAppBarState extends State<MyAppBar> {
  @override
  PreferredSizeWidget build(BuildContext context) {
    NeumorphicAppBar appBar = NeumorphicAppBar(
      leading: widget.leading ?? (kIsWeb ? const WebBackButton() : null),
      title: FittedBox(
        fit: BoxFit.fitWidth,
        child: Headline3(
          widget.title,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      actions: widget.actions,
      buttonStyle: const NeumorphicStyle(
        boxShape: NeumorphicBoxShape.circle(),
      ),
      iconTheme: IconThemeData(
        color: Theme.of(context).textTheme.headline3?.color,
      ),
      actionSpacing: 8,
    );

    if (widget.title == null) {
      return PreferredSize(
        preferredSize: const Size.fromHeight(0),
        child: Container(),
      );
    } else if (isMobile() || kIsWeb) {
      return PreferredSize(
        preferredSize: const Size.fromHeight(NeumorphicAppBar.toolbarHeight),
        child: Neumorphic(
          style: NeumorphicStyle(
              depth: widget.isScrolledToTop ? 0 : 10,
              boxShape: const NeumorphicBoxShape.rect()),
          child: widget.bottom == null
              ? appBar
              : Stack(
                  children: [
                    appBar,
                    Positioned(
                      bottom: 0,
                      width: MediaQuery.of(context).size.width,
                      child: widget.bottom!,
                    )
                  ],
                ),
        ),
      );
    } else {
      return MyDesktopAppBar(
        isScrolledToTop: widget.isScrolledToTop,
        actions: widget.actions,
        bottom: widget.bottom,
        leading: widget.leading,
        title: widget.title,
      );
    }
  }
}
