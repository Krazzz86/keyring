import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:keyring/utils/theme_preference.dart';
import 'package:provider/provider.dart';

import 'my_form_field.dart';
import 'my_neumorphic_toggle.dart';

class ThemePicker extends StatelessWidget {
  const ThemePicker({
    Key? key,
    this.label = true,
  }) : super(key: key);
  final bool label;

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return Consumer(
      builder: (context, SharedPreferencesModel sharedPreferences, child) {
        int selectedIndex = sharedPreferences.theme == ThemePreference.system
            ? 0
            : sharedPreferences.theme == ThemePreference.light
                ? 1
                : 2;
        return MyFormField(
          padding: EdgeInsets.zero,
          label: label ? localizations?.pageSettingsOptionThemeTitle : null,
          child: MyNeumorphicToggle(
            children: [
              '${localizations?.pageSettingsOptionThemeSystem}',
              '${localizations?.pageSettingsOptionThemeLight}',
              '${localizations?.pageSettingsOptionThemeDark}',
            ],
            selectedIndex: selectedIndex,
            onChanged: (newIndex) {
              switch (newIndex) {
                case 0:
                  sharedPreferences.theme = ThemePreference.system;
                  break;
                case 1:
                  sharedPreferences.theme = ThemePreference.light;
                  break;
                case 2:
                  sharedPreferences.theme = ThemePreference.dark;
                  break;
                default:
              }
            },
          ),
        );
      },
    );
  }
}
