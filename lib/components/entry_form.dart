import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_form_field.dart';
import 'package:keyring/components/my_neumorphic_toggle.dart';
import 'package:keyring/components/neumorphic_text_form_field.dart';
import 'package:keyring/components/row_with_button.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_type.dart';
import 'package:keyring/data/hash_algorithm.dart';

class EntryForm extends StatefulWidget {
  const EntryForm({Key? key, this.formKey, this.initialValues})
      : super(key: key);

  final GlobalKey<FormState>? formKey;
  final Entry? initialValues;

  @override
  EntryFormState createState() => EntryFormState();
}

Duration crossFadeDuration = const Duration(milliseconds: 250);

class EntryFormState extends State<EntryForm>
    with SingleTickerProviderStateMixin {
  EntryFormValue entry = EntryFormValue();
  int? selectedIndex = 1;
  int? typeIndex = 0;
  CrossFadeState crossFadeState = CrossFadeState.showFirst;
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: crossFadeDuration,
      vsync: this,
    );
    if (widget.initialValues != null) {
      int newIndex = 1;
      switch (widget.initialValues!.algorithm) {
        case HashAlgorithm.sha1:
          newIndex = 0;
          break;
        case HashAlgorithm.sha512:
          newIndex = 2;
          break;
        case HashAlgorithm.sha256:
          newIndex = 1;
          break;
      }
      int newTypeIndex = 0;
      switch (widget.initialValues!.type) {
        case EntryType.totp:
          newTypeIndex = 0;
          break;
        case EntryType.hotp:
          newTypeIndex = 1;
          break;
      }

      selectedIndex = newIndex;
      typeIndex = newTypeIndex;
      entry.algorithm = widget.initialValues!.algorithm;
      entry.type = widget.initialValues!.type;
    }
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _changeCrossFadeState() {
    setState(() {
      if (crossFadeState == CrossFadeState.showFirst) {
        crossFadeState = CrossFadeState.showSecond;
        _controller.forward();
      } else {
        crossFadeState = CrossFadeState.showFirst;
        _controller.reverse();
      }
    });
  }

  String? isEmpty(String? value) {
    if (value != null && value.isNotEmpty) {
      return null;
    } else {
      return '${AppLocalizations.of(context)?.componentEntryFormNotEmpty}';
    }
  }

  String? isInt(String? value) {
    if (value != null && int.tryParse(value) != null) {
      return null;
    } else {
      return '${AppLocalizations.of(context)?.componentEntryFormInteger}';
    }
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);
    EdgeInsets formFieldPadding = const EdgeInsets.fromLTRB(16, 16, 16, 0);

    return Form(
      key: widget.formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
          children: [
            MyFormField(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              label: localizations?.componentEntryFormType,
              child: MyNeumorphicToggle(
                children: const ['TOTP', 'HOTP'],
                selectedIndex: typeIndex,
                onChanged: (newIndex) {
                  setState(() {
                    typeIndex = newIndex;
                  });
                  switch (newIndex) {
                    case 0:
                      entry.type = EntryType.totp;
                      break;
                    case 1:
                      entry.type = EntryType.hotp;
                      break;
                    default:
                      break;
                  }
                },
              ),
            ),
            NeumorphicTextFormField(
              padding: formFieldPadding,
              label: localizations?.componentEntryFormIssuer,
              initialValue: widget.initialValues?.issuer,
              onSaved: (String? value) {
                setState(() {
                  entry.issuer = value;
                });
              },
            ),
            NeumorphicTextFormField(
              padding: formFieldPadding,
              label: localizations?.componentEntryFormLabel,
              initialValue: widget.initialValues?.label,
              validator: isEmpty,
              onSaved: (String? value) {
                setState(() {
                  entry.label = value;
                });
              },
            ),
            Visibility(
              visible: widget.initialValues == null,
              child: NeumorphicTextFormField(
                padding: formFieldPadding,
                label: localizations?.componentEntryFormSecret,
                validator: isEmpty,
                initialValue: widget.initialValues?.secret,
                onSaved: (String? value) {
                  setState(() {
                    entry.secret = value;
                  });
                },
              ),
            ),
            RowWithButton(
              label: localizations?.componentEntryFormAdvanced,
              child: RotationTransition(
                turns: Tween(begin: -0.25, end: 0.25).animate(_controller),
                child: Icon(
                  Icons.chevron_left_rounded,
                  color: Theme.of(context).textTheme.headline3?.color,
                ),
              ),
              onPressed: _changeCrossFadeState,
              id: 'component_entry_form_advanced',
            ),
            AnimatedCrossFade(
              firstChild: Container(),
              secondChild: Column(
                children: [
                  AnimatedCrossFade(
                    firstChild: NeumorphicTextFormField(
                      padding: formFieldPadding,
                      label: localizations?.componentEntryFormPeriod,
                      keyboardType: const TextInputType.numberWithOptions(),
                      validator: isInt,
                      initialValue: widget.initialValues?.period.toString() ??
                          entry.period.toString(),
                      onSaved: (String? value) {
                        if (value == null) {
                          setState(() {
                            entry.period = null;
                          });
                        } else {
                          int? period = int.tryParse(value);
                          period ??= 30;
                          setState(() {
                            entry.period = period;
                          });
                        }
                      },
                    ),
                    secondChild: NeumorphicTextFormField(
                      padding: formFieldPadding,
                      label: localizations?.componentEntryFormCounter,
                      keyboardType: const TextInputType.numberWithOptions(),
                      validator: isInt,
                      initialValue:
                          ((widget.initialValues?.counter ?? entry.counter) ??
                                  0)
                              .toString(),
                      onSaved: (String? value) {
                        if (value == null) {
                          setState(() {
                            entry.counter = null;
                          });
                        } else {
                          int? counter = int.tryParse(value);
                          counter ??= 0;
                          setState(() {
                            entry.counter = counter;
                          });
                        }
                      },
                    ),
                    crossFadeState: typeIndex == 0
                        ? CrossFadeState.showFirst
                        : CrossFadeState.showSecond,
                    duration: crossFadeDuration,
                  ),
                  NeumorphicTextFormField(
                    padding: formFieldPadding,
                    label: localizations?.componentEntryFormDigits,
                    keyboardType: const TextInputType.numberWithOptions(),
                    validator: isInt,
                    initialValue: widget.initialValues?.digits.toString() ??
                        entry.digits.toString(),
                    onSaved: (String? value) {
                      if (value == null) {
                        setState(() {
                          entry.digits = null;
                        });
                      } else {
                        int? digits = int.tryParse(value);
                        digits ??= 6;
                        setState(() {
                          entry.digits = digits;
                        });
                      }
                    },
                  ),
                  MyFormField(
                    padding: formFieldPadding,
                    label: localizations?.componentEntryFormAlgorythm,
                    child: MyNeumorphicToggle(
                      children: const ['SHA1', 'SHA256', 'SHA512'],
                      selectedIndex: selectedIndex,
                      onChanged: (newIndex) {
                        setState(() {
                          selectedIndex = newIndex;
                        });
                        switch (newIndex) {
                          case 0:
                            entry.algorithm = HashAlgorithm.sha1;
                            break;
                          case 1:
                            entry.algorithm = HashAlgorithm.sha256;
                            break;
                          case 2:
                            entry.algorithm = HashAlgorithm.sha512;
                            break;
                          default:
                        }
                      },
                    ),
                  )
                ],
              ),
              crossFadeState: crossFadeState,
              duration: crossFadeDuration,
            ),
          ],
        ),
      ),
    );
  }
}

class EntryFormValue {
  int? id;
  String? secret;
  String? label;
  String? issuer;
  int? period = 30;
  int? digits = 6;
  int? counter;
  HashAlgorithm? algorithm = HashAlgorithm.sha256;
  String? thumbnail;
  int? lastUsed;
  EntryType type = EntryType.totp;
}
