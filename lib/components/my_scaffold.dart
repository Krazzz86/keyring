import 'dart:io';

import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_app_bar.dart';
import 'package:keyring/utils/is_mobile.dart';

class MyScaffold extends StatefulWidget {
  const MyScaffold({
    Key? key,
    this.body,
    this.leading,
    this.actions,
    this.floatingActionButton,
    this.scrollController,
    this.title,
    this.bottom,
    this.safeArea = true,
  }) : super(key: key);

  final Widget? body;
  final Widget? floatingActionButton;
  final ScrollController? scrollController;
  final String? title;
  final Widget? leading;
  final List<Widget>? actions;
  final Widget? bottom;
  final bool safeArea;

  @override
  _MyScaffoldState createState() => _MyScaffoldState();
}

class _MyScaffoldState extends State<MyScaffold> with TickerProviderStateMixin {
  static const double emptySpace = 0;
  bool isScrolledToTop = true;
  double previousOffset = 0;
  bool showFAB = true;
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 30),
    vsync: this,
  )..repeat();

  void _scrollListener() {
    if (widget.scrollController != null) {
      if (widget.scrollController!.offset <=
              widget.scrollController!.position.minScrollExtent &&
          !widget.scrollController!.position.outOfRange) {
        //call setState only when values are about to change
        if (!isScrolledToTop) {
          setState(() {
            //reach the top
            isScrolledToTop = true;
          });
        }
      } else {
        //call setState only when values are about to change
        if (widget.scrollController!.offset > emptySpace && isScrolledToTop) {
          if (isScrolledToTop) {
            setState(() {
              //not the top
              isScrolledToTop = false;
              showFAB = false;
            });
          }
        }
      }

      setState(() {
        showFAB = previousOffset > widget.scrollController!.offset;
      });

      setState(() {
        previousOffset = widget.scrollController!.offset;
      });
    }
  }

  @override
  void initState() {
    widget.scrollController?.addListener(_scrollListener);
    super.initState();
  }

  bool _isMaximized() {
    try {
      return Platform.isLinux && appWindow.isMaximized;
    } catch (e) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(
        isMobile() || kIsWeb || _isMaximized() ? 0 : 24.0,
      ),
      child: Scaffold(
        appBar: MyAppBar(
          isScrolledToTop: isScrolledToTop,
          actions: widget.actions,
          bottom: widget.bottom,
          leading: widget.leading,
          title: widget.title,
        ),
        body: Align(
          alignment: Alignment.topCenter,
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 1000),
            child: widget.safeArea && widget.body != null
                ? SafeArea(child: widget.body!)
                : widget.body,
          ),
        ),
        floatingActionButton:
            MediaQuery.of(context).size.width > 1200 || showFAB
                ? widget.floatingActionButton
                : null,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    widget.scrollController?.removeListener(_scrollListener);
    super.dispose();
  }
}
