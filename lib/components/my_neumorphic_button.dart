import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/services/analytics.dart';

class MyNeumorphicButton extends StatelessWidget {
  const MyNeumorphicButton({
    required this.text,
    required this.id,
    Key? key,
    this.onPressed,
    this.icon,
  }) : super(key: key);
  final String? text;
  final Function()? onPressed;
  final IconData? icon;
  final String id;

  @override
  Widget build(BuildContext context) {
    Widget child = icon != null
        ? Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(
                  icon,
                  color: Theme.of(context).textTheme.headline3?.color,
                ),
              ),
              Button(text),
            ],
          )
        : Button(text);
    return NeumorphicButton(
      child: child,
      minDistance: -5,
      style: const NeumorphicStyle(
        boxShape: NeumorphicBoxShape.stadium(),
      ),
      onPressed: () {
        AnalyticsService.logClick(id: id);
        onPressed?.call();
      },
    );
  }
}
