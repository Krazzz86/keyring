import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/typography.dart';

import 'my_neumorphic_icon_button.dart';

class MyDesktopAppBar extends StatefulWidget implements PreferredSizeWidget {
  const MyDesktopAppBar({
    required this.isScrolledToTop,
    this.leading,
    this.actions,
    this.title,
    this.bottom,
    Key? key,
  }) : super(key: key);
  final String? title;
  final Widget? leading;
  final List<Widget>? actions;
  final Widget? bottom;
  final bool isScrolledToTop;

  @override
  _MyDesktopAppBarState createState() => _MyDesktopAppBarState();

  @override
  Size get preferredSize {
    if (title == null) {
      return const Size.fromHeight(0);
    } else {
      return const Size.fromHeight(NeumorphicAppBar.toolbarHeight);
    }
  }
}

class _MyDesktopAppBarState extends State<MyDesktopAppBar> {
  @override
  Widget build(BuildContext context) {
    final ModalRoute<dynamic>? parentRoute = ModalRoute.of(context);
    final bool canPop = parentRoute?.canPop ?? false;
    final bool useCloseButton =
        parentRoute is PageRoute<dynamic> && parentRoute.fullscreenDialog;

    Widget leading = widget.leading != null
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: widget.leading,
          )
        : (canPop
            ? Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: useCloseButton
                    ? MyNeumorphicIconButton(
                        id: 'back',
                        icon: Icons.close,
                        onPressed: () {
                          Navigator.maybePop(context);
                        },
                      )
                    : MyNeumorphicIconButton(
                        id: 'close',
                        icon: Icons.arrow_back,
                        onPressed: () {
                          Navigator.maybePop(context);
                        },
                      ),
              )
            : Container());

    int trailingLength = (widget.actions?.length ?? 0) + 3;

    List<Widget> trailing = [
      ...?widget.actions,
      MyNeumorphicIconButton(
        icon: Icons.minimize_rounded,
        onPressed: () {
          appWindow.minimize();
        },
        id: 'app_window_minimize',
      ),
      MyNeumorphicIconButton(
        icon: appWindow.isMaximized
            ? Icons.fullscreen_exit_rounded
            : Icons.fullscreen_rounded,
        onPressed: () {
          appWindow.maximizeOrRestore();
        },
        id: 'app_window_maximize_or_restore',
      ),
      MyNeumorphicIconButton(
        icon: Icons.close_rounded,
        onPressed: () {
          appWindow.close();
        },
        id: 'app_window_close',
      ),
    ]
        .asMap()
        .map((index, entry) {
          return MapEntry(
            index,
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: index + 1 == trailingLength ? 0 : 8),
              child: entry,
            ),
          );
        })
        .values
        .toList();

    Widget appBar = Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // if (widget.leading != null) ...[widget.leading]
          leading,
          Expanded(
              child: MoveWindow(
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Headline3(
                widget.title,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          )),
          ...trailing,
        ],
      ),
    );

    return PreferredSize(
      preferredSize: const Size.fromHeight(NeumorphicAppBar.toolbarHeight),
      child: Neumorphic(
        style: NeumorphicStyle(
            depth: widget.isScrolledToTop ? 0 : 10,
            boxShape: const NeumorphicBoxShape.rect()),
        child: widget.bottom == null
            ? appBar
            : Stack(
                children: [
                  appBar,
                  Positioned(
                    bottom: 0,
                    width: MediaQuery.of(context).size.width,
                    child: widget.bottom!,
                  )
                ],
              ),
      ),
    );
  }
}
