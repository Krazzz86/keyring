import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_form_field.dart';

class NeumorphicTextFormField extends StatelessWidget {
  const NeumorphicTextFormField({
    required this.label,
    Key? key,
    this.validator,
    this.onSaved,
    this.keyboardType,
    this.initialValue,
    this.padding,
    this.onChanged,
  }) : super(key: key);
  final String? label;
  final FormFieldValidator<String>? validator;
  final FormFieldSetter<String>? onSaved;
  final TextInputType? keyboardType;
  final String? initialValue;
  final EdgeInsets? padding;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return MyFormField(
      label: label,
      padding: padding,
      child: Neumorphic(
        style: const NeumorphicStyle().copyWith(
          depth: -5,
          boxShape: const NeumorphicBoxShape.stadium(),
        ),
        child: TextFormField(
          initialValue: initialValue,
          decoration: const InputDecoration(
            hintText: '',
            contentPadding: EdgeInsets.all(16.0),
            border: InputBorder.none,
          ),
          validator: validator,
          keyboardType: keyboardType,
          onChanged: onChanged,
          onSaved: onSaved,
          style: Theme.of(context).textTheme.subtitle1?.copyWith(
                color: Theme.of(context).textTheme.headline3?.color,
              ),
        ),
      ),
    );
  }
}
