import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/app_bar_progress.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:keyring/data/entry_type.dart';
import 'package:keyring/pages/edit/edit.dart';
import 'package:keyring/services/analytics.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class EntryTile extends StatefulWidget {
  const EntryTile({
    required Key key,
    required this.token,
    required this.entry,
    required this.stream,
    required this.showProgressBar,
    required this.progressBarPeriod,
    required this.showIssuer,
  }) : super(key: key);
  final Entry entry;
  final String token;
  final Stream<int> stream;
  final bool showProgressBar;
  final bool showIssuer;
  final int progressBarPeriod;

  @override
  _EntryTileState createState() => _EntryTileState();
}

class _EntryTileState extends State<EntryTile> {
  final GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);
    bool hasIssuer = widget.showIssuer &&
        widget.entry.issuer != null &&
        widget.entry.issuer!.isNotEmpty;

    List<Widget> children = [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Consumer(
                  builder: (context, SharedPreferencesModel sharedPreferences,
                      child) {
                    return Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Opacity(
                            opacity: hasIssuer ? 0.5 : 1.0,
                            child: Headline4(
                              hasIssuer
                                  ? widget.entry.issuer!
                                  : widget.entry.label,
                              overflow: TextOverflow.clip,
                              maxLines: hasIssuer
                                  ? sharedPreferences.issuerMaxNumberOfLines
                                  : sharedPreferences.labelMaxNumberOfLines,
                            ),
                          ),
                          hasIssuer
                              ? Headline4(
                                  widget.entry.label,
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.left,
                                  softWrap:
                                      sharedPreferences.labelMaxNumberOfLines !=
                                          1,
                                  maxLines:
                                      sharedPreferences.labelMaxNumberOfLines,
                                )
                              : Container(),
                        ],
                      ),
                    );
                  },
                ),
                widget.entry.type == EntryType.hotp
                    ? Consumer(
                        builder: (context, EntryModel entryModel, child) {
                          return MyNeumorphicIconButton(
                            tooltip: localizations?.componentEntryTileGenerate,
                            icon: Icons.play_arrow_rounded,
                            onPressed: () {
                              widget.entry.counter++;
                              entryModel.edit(widget.entry);
                            },
                            id: 'component_entry_tile_generate',
                          );
                        },
                      )
                    : Container(),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: widget.token.characters
                  .map(
                    (c) => Headline2(c),
                  )
                  .toList(),
            )
          ],
        ),
      ),
    ];

    if (widget.showProgressBar &&
        widget.entry.period != widget.progressBarPeriod) {
      children.add(
        AppBarProgress(
          stream: widget.stream,
          period: widget.entry.period,
        ),
      );
    }

    return InkWell(
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      onTap: () {
        AnalyticsService.logClick(id: 'component_entry_tile');
        Navigator.pushNamed(
          context,
          EditPage.routeName,
          arguments: EditPageArguments(widget.entry),
        );
      },
      onLongPress: () {
        AnalyticsService.logLongPress(id: 'component_entry_tile');
        Clipboard.setData(ClipboardData(text: widget.token));
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: SnackBarText(
              localizations?.componentEntryTileCopiedCode(widget.token),
            ),
          ),
        );
      },
      child: Column(
        children: children,
      ),
    );
  }
}
