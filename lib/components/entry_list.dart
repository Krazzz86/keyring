import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

import 'entry_tile.dart';

class EntryList extends StatefulWidget {
  const EntryList({required this.stream, this.scrollController, Key? key})
      : super(key: key);
  final Stream<int> stream;
  final ScrollController? scrollController;

  @override
  _EntryListState createState() => _EntryListState();
}

class _EntryListState extends State<EntryList> {
  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return Consumer(
      builder: (context, SharedPreferencesModel sharedPreferences, child) {
        return Consumer(
          builder: (context, EntryModel entryModel, child) {
            if (entryModel.entries == null) {
              return Container();
            }
            if (entryModel.entries!.isEmpty) {
              return Center(
                child: Headline2(
                  localizations?.pageHomeBodyEmpty,
                  textAlign: TextAlign.center,
                ),
              );
            }
            return StreamBuilder<int>(
              stream: widget.stream,
              builder: (context, snapshot) {
                return ListView.separated(
                  controller: widget.scrollController,
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).padding.bottom + 80),
                  itemBuilder: (BuildContext context, int index) {
                    Entry entry = entryModel.entries![index];
                    return EntryTile(
                      key: Key(entry.hashCode.toString()),
                      entry: entry,
                      token: entry.calculateToken(),
                      stream: widget.stream,
                      showProgressBar:
                          sharedPreferences.showIndividualProgressBar,
                      progressBarPeriod:
                          sharedPreferences.individualProgressBarPeriod,
                      showIssuer: sharedPreferences.showIssuer,
                    );
                  },
                  itemCount: entryModel.entries!.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider(
                      height: 1,
                      thickness: 1,
                    );
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}
