import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class Headline2 extends StatelessWidget {
  const Headline2(
    this.data, {
    Key? key,
    this.textAlign,
  }) : super(key: key);
  final String? data;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context)
          .textTheme
          .headline2
          ?.copyWith(fontFamily: 'Comfortaa'),
      textAlign: textAlign,
    );
  }
}

class Headline3 extends StatelessWidget {
  const Headline3(
    this.data, {
    Key? key,
    this.overflow,
    this.maxLines,
  }) : super(key: key);
  final String? data;
  final TextOverflow? overflow;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context)
          .textTheme
          .headline3
          ?.copyWith(fontFamily: 'Comfortaa'),
      overflow: overflow,
      maxLines: maxLines,
    );
  }
}

class Headline4 extends StatelessWidget {
  const Headline4(
    this.data, {
    Key? key,
    this.overflow,
    this.maxLines,
    this.textAlign,
    this.softWrap,
  }) : super(key: key);
  final String? data;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final int? maxLines;
  final bool? softWrap;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context)
          .textTheme
          .headline4
          ?.copyWith(fontFamily: 'Comfortaa'),
      overflow: overflow,
      maxLines: maxLines,
      textAlign: textAlign,
      softWrap: softWrap,
    );
  }
}

class BodyText1 extends StatelessWidget {
  const BodyText1(
    this.data, {
    Key? key,
    this.textAlign,
    this.fontSize,
  }) : super(key: key);
  final String? data;
  final TextAlign? textAlign;
  final double? fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context).textTheme.bodyText1?.copyWith(
            color: Theme.of(context).textTheme.headline3?.color,
            fontSize: fontSize,
            fontFamily: 'FiraSans',
          ),
      textAlign: textAlign,
    );
  }
}

class SnackBarText extends StatelessWidget {
  const SnackBarText(
    this.data, {
    Key? key,
    this.textAlign,
    this.fontSize,
  }) : super(key: key);
  final String? data;
  final TextAlign? textAlign;
  final double? fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context).textTheme.bodyText1?.copyWith(
            color: NeumorphicTheme.of(context)?.current?.baseColor,
            fontSize: fontSize,
          ),
      textAlign: textAlign ?? TextAlign.center,
    );
  }
}

class Subtitle1 extends StatelessWidget {
  const Subtitle1(
    this.data, {
    Key? key,
  }) : super(key: key);
  final String? data;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context).textTheme.subtitle1?.copyWith(
            color: Theme.of(context).textTheme.headline3?.color,
            fontFamily: 'FiraSans',
          ),
    );
  }
}

class Button extends StatelessWidget {
  const Button(
    this.data, {
    Key? key,
  }) : super(key: key);
  final String? data;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$data',
      style: Theme.of(context).textTheme.button?.copyWith(
            color: Theme.of(context).textTheme.headline3?.color,
            fontFamily: 'FiraSans',
          ),
    );
  }
}
