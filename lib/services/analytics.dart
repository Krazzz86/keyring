import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:keyring/utils/is_mobile.dart';

mixin AnalyticsService {
  static logClick({
    required String id,
  }) {
    logEvent(name: 'click', parameters: {'id': id});
  }

  static logLongPress({
    required String id,
  }) {
    logEvent(name: 'long_press', parameters: {'id': id});
  }

  static logEvent({
    required String name,
    Map<String, Object?>? parameters,
  }) {
    if (isMobile()) {
      FirebaseAnalytics().logEvent(name: name, parameters: parameters);
    }
  }

  static setProperty({
    required String name,
    required String value,
  }) {
    if (isMobile()) {
      FirebaseAnalytics().setUserProperty(name: name, value: value);
    }
  }
}
