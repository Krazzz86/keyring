import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:keyring/components/app_bar_progress.dart';
import 'package:keyring/components/entry_list.dart';
import 'package:keyring/components/floating_action_button_column.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:keyring/pages/settings/settings.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:keyring/utils/route_observer.dart';
import 'package:keyring/utils/set_navbar.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  static const routeName = '/home';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with RouteAware {
  final ScrollController _scrollController = ScrollController();
  final Stream<int> _stream = Stream.periodic(
    const Duration(milliseconds: 10),
    (int count) => DateTime.now().millisecondsSinceEpoch,
  ).asBroadcastStream();

  _addFlags() async {
    if (await _getPrivacyPreference() && isMobile()) {
      await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
    }
  }

  _clearFlags() async {
    if (await _getPrivacyPreference() && isMobile()) {
      await FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }
  }

  @override
  void didChangeDependencies() {
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
    super.didChangeDependencies();
  }

  Future<bool> _getPrivacyPreference() async {
    return (await SharedPreferencesModel().getPreferences()).privateHomeScreen;
  }

  @override
  void initState() {
    if (!kIsWeb) {
      _addFlags();
    }
    super.initState();
  }

  /// Called when the current route has been pushed.
  @override
  void didPush() {
    _addFlags();
  }

  /// Called when the top route has been popped off, and the current route
  /// shows up.
  @override
  void didPopNext() {
    _addFlags();
  }

  /// Called when the current route has been popped off.
  @override
  void didPop() {
    _clearFlags();
  }

  /// Called when a new route has been pushed, and the current route is no
  /// longer visible.
  @override
  void didPushNext() {
    _clearFlags();
  }

  @override
  Widget build(BuildContext context) {
    setNavbar(context);
    AppLocalizations? localizations = AppLocalizations.of(context);

    return ClipRRect(
      child: MyScaffold(
        scrollController: _scrollController,
        title: localizations?.pageHomeTitle,
        safeArea: false,
        leading: Center(
          child: Icon(
            Icons.vpn_key,
            color: Theme.of(context).textTheme.headline3?.color,
            size: 32,
          ),
        ),
        actions: [
          MyNeumorphicIconButton(
            tooltip: localizations?.pageHomeSettingsTooltip,
            icon: Icons.settings_rounded,
            onPressed: () {
              Navigator.of(context).pushNamed(SettingsPage.routeName);
            },
            id: 'page_home_settings',
          )
        ],
        bottom: Consumer(
          builder: (context, SharedPreferencesModel sharedPreferences, child) {
            return Consumer(
              builder: (context, EntryModel entryModel, child) {
                return sharedPreferences.showGlobalProgressBar &&
                        entryModel.entries != null &&
                        entryModel.entries!.isNotEmpty
                    ? AppBarProgress(
                        stream: _stream,
                        period: sharedPreferences.globalProgressBarPeriod,
                      )
                    : Container();
              },
            );
          },
        ),
        body: EntryList(
          stream: _stream,
          scrollController: _scrollController,
        ),
        floatingActionButton: const FloatingActionButtonColumn(),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _clearFlags();
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
