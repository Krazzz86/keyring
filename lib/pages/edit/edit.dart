import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/entry_form.dart';
import 'package:keyring/components/my_neumorphic_button.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:keyring/pages/share/share.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:keyring/utils/theme_preference.dart';
import 'package:provider/provider.dart';

class EditPageArguments {
  EditPageArguments(this.entry);
  final Entry entry;
}

class EditPage extends StatefulWidget {
  const EditPage({Key? key}) : super(key: key);
  static const routeName = '/edit';

  @override
  _EditPageState createState() => _EditPageState();
}

const crossFadeDuration = Duration(milliseconds: 250);

class _EditPageState extends State<EditPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<EntryFormState> formWidgetKey = GlobalKey<EntryFormState>();
  final ScrollController _scrollController = ScrollController();
  CrossFadeState _crossFadeState = CrossFadeState.showFirst;
  int? _warningId;
  String _warningTitle = '';
  String _warningBody = '';
  Function()? _afterConfirm;

  @override
  void initState() {
    super.initState();
  }

  _showWarning({
    required int id,
    required String warningTitle,
    required Function() afterConfirm,
    String? warningBody,
  }) {
    if (_crossFadeState == CrossFadeState.showFirst || _warningId != id) {
      setState(() {
        _warningId = id;
        _warningTitle = warningTitle;
        _warningBody = warningBody ?? '';
        _afterConfirm = afterConfirm;
        _crossFadeState = _crossFadeState = CrossFadeState.showSecond;
      });
    } else {
      setState(() {
        _warningId = null;
        _warningTitle = '';
        _warningBody = '';
        _afterConfirm = null;
        _crossFadeState = _crossFadeState = CrossFadeState.showFirst;
      });
    }
    _scrollController.animateTo(
      0,
      duration: crossFadeDuration,
      curve: Curves.easeInOut,
    );
  }

  _hideWarning() {
    setState(() {
      _crossFadeState = CrossFadeState.showFirst;
    });
  }

  @override
  Widget build(BuildContext context) {
    final EditPageArguments args =
        ModalRoute.of(context)?.settings.arguments as EditPageArguments;
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageEditTitle,
      scrollController: _scrollController,
      actions: [
        MyNeumorphicIconButton(
          tooltip: '${localizations?.pageEditShareTooltip}',
          icon: Icons.share_rounded,
          onPressed: () {
            _showWarning(
              id: 0,
              warningTitle: '${localizations?.pageEditShareWarningTitle}',
              warningBody: localizations?.pageEditShareWarningBody,
              afterConfirm: () async {
                Provider.of<SharedPreferencesModel>(context, listen: false)
                    .setThemeWithoutSaving(ThemePreference.light);
                await Navigator.pushNamed(
                  context,
                  SharePage.routeName,
                  arguments: SharePageArguments(args.entry),
                );
                Provider.of<SharedPreferencesModel>(context, listen: false)
                    .getPreferences();
              },
            );
          },
          id: 'page_edit_share',
        ),
      ],
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: [
            AnimatedCrossFade(
              firstChild: Column(
                children: [
                  EntryForm(
                    formKey: formKey,
                    key: formWidgetKey,
                    initialValues: args.entry,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Consumer(
                          builder: (context, EntryModel entryModel, child) {
                            return MyNeumorphicButton(
                              text: '${localizations?.pageEditRemoveTooltip}',
                              onPressed: () {
                                _showWarning(
                                  id: 1,
                                  warningTitle:
                                      '${localizations?.pageEditRemoveWarningTitle}',
                                  warningBody:
                                      localizations?.pageEditRemoveWarningBody,
                                  afterConfirm: () async {
                                    await entryModel.remove(args.entry);
                                    Navigator.pop(context, args.entry);
                                  },
                                );
                              },
                              id: 'page_edit_remove',
                            );
                          },
                        ),
                        Consumer(
                          builder: (context, EntryModel entryModel, child) {
                            return MyNeumorphicButton(
                              text: '${localizations?.pageEditSaveTooltip}',
                              onPressed: () {
                                _showWarning(
                                  id: 2,
                                  warningTitle:
                                      '${localizations?.pageEditSaveWarningTitle}',
                                  warningBody:
                                      localizations?.pageEditSaveWarningBody,
                                  afterConfirm: () async {
                                    bool? valid =
                                        formKey.currentState?.validate();
                                    if (valid != null && valid) {
                                      formKey.currentState?.save();
                                      final dynamic value =
                                          formWidgetKey.currentState?.entry;
                                      value.id = args.entry.id;
                                      value.secret = args.entry.secret;
                                      final Entry? entry =
                                          Entry.tryFromDynamic(value: value);
                                      if (entry != null) {
                                        await entryModel.edit(entry);
                                        Navigator.pop(context, entry);
                                      }
                                    }
                                  },
                                );
                              },
                              id: 'page_edit_save',
                            );
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
              secondChild: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Headline4(_warningTitle),
                  ),
                  _warningBody.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: BodyText1(_warningBody),
                        )
                      : Container(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        MyNeumorphicButton(
                          text: '${localizations?.pageEditRemoveWarningCancel}',
                          onPressed: () {
                            _hideWarning();
                          },
                          id: 'page_edit_warning_cancel',
                        ),
                        MyNeumorphicButton(
                          text:
                              '${localizations?.pageEditRemoveWarningConfirm}',
                          onPressed: () {
                            _hideWarning();
                            _afterConfirm?.call();
                          },
                          id: 'page_edit_warning_confirm',
                        )
                      ],
                    ),
                  )
                ],
              ),
              crossFadeState: _crossFadeState,
              duration: crossFadeDuration,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
