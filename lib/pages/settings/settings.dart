import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/row_with_button.dart';
import 'package:keyring/pages/settings/pages/about.dart';
import 'package:keyring/pages/settings/pages/appearance.dart';
import 'package:keyring/pages/settings/pages/backup.dart';
import 'package:keyring/pages/settings/pages/language.dart';
import 'package:keyring/pages/settings/pages/privacy.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:keyring/utils/rate.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);
  static const routeName = '/settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    rate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);
    return MyScaffold(
      title: localizations?.pageSettingsTitle,
      scrollController: _scrollController,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          ListView(
            shrinkWrap: true,
            controller: _scrollController,
            children: [
              RowWithButton(
                label: localizations?.pageSettingsOptionAppearanceTitle,
                icon: Icons.chevron_right_rounded,
                onPressed: () {
                  Navigator.of(context).pushNamed(AppearancePage.routeName);
                },
                id: 'page_settings_option_appearance',
              ),
              RowWithButton(
                label: localizations?.pageSettingsOptionLanguageTitle,
                icon: Icons.chevron_right_rounded,
                onPressed: () {
                  Navigator.of(context).pushNamed(LanguagePage.routeName);
                },
                id: 'page_settings_option_language',
              ),
              if (isMobile()) ...[
                RowWithButton(
                  label: localizations?.pageSettingsOptionPrivacyTitle,
                  icon: Icons.chevron_right_rounded,
                  onPressed: () {
                    Navigator.of(context).pushNamed(PrivacyPage.routeName);
                  },
                  id: 'page_settings_option_privacy',
                )
              ],
              RowWithButton(
                label: localizations?.pageSettingsOptionBackupTitle,
                icon: Icons.chevron_right_rounded,
                onPressed: () {
                  Navigator.of(context).pushNamed(BackupPage.routeName);
                },
                id: 'page_settings_option_backup',
              )
            ],
          ),
          RowWithButton(
            label: localizations?.pageSettingsOptionAboutTitle,
            icon: Icons.chevron_right_rounded,
            onPressed: () {
              Navigator.of(context).pushNamed(AboutPage.routeName);
            },
            id: 'page_settings_option_about',
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
