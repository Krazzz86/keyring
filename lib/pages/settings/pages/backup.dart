import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_neumorphic_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/utils/export_backup.dart';
import 'package:keyring/utils/import_backup.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class BackupPage extends StatefulWidget {
  const BackupPage({Key? key}) : super(key: key);
  static const routeName = '/settings/backup';

  @override
  _BackupPageState createState() => _BackupPageState();
}

class _BackupPageState extends State<BackupPage> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageSettingsOptionBackupTitle,
      scrollController: _scrollController,
      body: Consumer(
        builder: (context, SharedPreferencesModel sharedPreferences, child) {
          return ListView(
            padding: const EdgeInsets.all(16.0),
            controller: _scrollController,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 32),
                child: BodyText1(
                  localizations?.pageSettingsOptionBackupImportBody,
                  fontSize: 20,
                ),
              ),
              Center(
                child: MyNeumorphicButton(
                  icon: Icons.file_download_rounded,
                  text: localizations?.pageSettingsOptionBackupImport,
                  onPressed: () {
                    importBackup(context);
                  },
                  id: 'page_settings_option_backup_import',
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32),
                child: BodyText1(
                  localizations?.pageSettingsOptionBackupExportBody,
                  fontSize: 20,
                ),
              ),
              Center(
                child: MyNeumorphicButton(
                  icon: Icons.file_upload_rounded,
                  text: localizations?.pageSettingsOptionBackupExport,
                  onPressed: () {
                    exportBackup(context);
                  },
                  id: 'page_settings_option_backup_export',
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
