import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_neumorphic_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/typography.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({Key? key}) : super(key: key);
  static const routeName = '/settings/about';

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);
    return MyScaffold(
      title: localizations?.pageSettingsOptionAboutTitle,
      scrollController: _scrollController,
      body: FutureBuilder(
        future: PackageInfo.fromPlatform(),
        builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
          if (snapshot.hasError || !snapshot.hasData) {
            return Container();
          }
          Widget icon = NeumorphicIcon(
            Icons.vpn_key,
            size: 56,
            style: NeumorphicStyle(
                color: Theme.of(context).textTheme.headline3?.color),
          );
          return ListView(
            controller: _scrollController,
            children: [
              icon,
              Headline2(
                localizations?.appName,
                textAlign: TextAlign.center,
              ),
              Headline4(
                snapshot.data?.version,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: MyNeumorphicButton(
                  text: '${localizations?.pageAboutLicences}',
                  onPressed: () {
                    showLicensePage(
                      context: context,
                      applicationIcon: icon,
                      applicationVersion: snapshot.data?.version,
                      applicationName: localizations?.appName,
                    );
                  },
                  id: 'page_about_licences',
                ),
              )
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
