import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class LanguagePage extends StatefulWidget {
  const LanguagePage({Key? key}) : super(key: key);
  static const routeName = '/settings/language';

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  final ScrollController _scrollController = ScrollController();

  _changeLanguage(
    SharedPreferencesModel sharedPreferences,
    BuildContext context,
    String? locale,
  ) {
    sharedPreferences.locale = locale != null ? Locale(locale) : null;
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageSettingsOptionLanguageTitle,
      scrollController: _scrollController,
      body: Consumer(
        builder: (context, SharedPreferencesModel sharedPreferences, child) {
          return ListView.separated(
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                String? title =
                    localizations?.pageSettingsOptionLanguageDefault;
                String? languageTag;

                if (index != 0) {
                  languageTag = AppLocalizations.supportedLocales[index - 1]
                      .toLanguageTag();
                  switch (languageTag) {
                    case 'en':
                      title = localizations?.pageSettingsOptionLanguageEnglish;
                      break;
                    case 'pl':
                      title = localizations?.pageSettingsOptionLanguagePolish;
                      break;
                    default:
                      title = localizations?.pageSettingsOptionLanguageDefault;
                  }
                }

                return ListTile(
                  title: Subtitle1(
                    title,
                  ),
                  onTap: () {
                    _changeLanguage(sharedPreferences, context, languageTag);
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return const Divider(
                  height: 1,
                  thickness: 1,
                );
              },
              itemCount: AppLocalizations.supportedLocales.length + 1);
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
