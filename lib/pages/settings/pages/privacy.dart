import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/switch_with_headline.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class PrivacyPage extends StatefulWidget {
  const PrivacyPage({Key? key}) : super(key: key);
  static const routeName = '/settings/privacy';

  @override
  _PrivacyPageState createState() => _PrivacyPageState();
}

class _PrivacyPageState extends State<PrivacyPage> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageSettingsOptionPrivacyTitle,
      scrollController: _scrollController,
      body: Consumer(
        builder: (context, SharedPreferencesModel sharedPreferences, child) {
          return ListView(
            padding: const EdgeInsets.all(16.0),
            controller: _scrollController,
            children: [
              SwitchWithHeadline(
                label: localizations?.pageSettingsOptionPrivateHomeTitle,
                value: sharedPreferences.privateHomeScreen,
                onChanged: (newValue) {
                  sharedPreferences.privateHomeScreen = newValue;
                },
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
