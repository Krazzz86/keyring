import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_form_field.dart';
import 'package:keyring/components/my_neumorphic_toggle.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/neumorphic_text_form_field.dart';
import 'package:keyring/components/switch_with_headline.dart';
import 'package:keyring/components/theme_picker.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

Duration crossFadeDuration = const Duration(milliseconds: 250);

class AppearancePage extends StatefulWidget {
  const AppearancePage({Key? key}) : super(key: key);
  static const routeName = '/settings/appearance';

  @override
  _AppearancePageState createState() => _AppearancePageState();
}

class _AppearancePageState extends State<AppearancePage> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageSettingsOptionAppearanceTitle,
      scrollController: _scrollController,
      body: Consumer(
        builder: (context, SharedPreferencesModel sharedPreferences, child) {
          return ListView(
            padding: const EdgeInsets.all(16.0),
            controller: _scrollController,
            children: [
              const ThemePicker(),
              SwitchWithHeadline(
                label: localizations?.pageSettingsOptionShowIssuerTitle,
                value: sharedPreferences.showIssuer,
                onChanged: (newValue) {
                  sharedPreferences.showIssuer = newValue;
                },
              ),
              AnimatedCrossFade(
                firstChild: MyFormField(
                  label: localizations?.pageSettingsOptionIssuerTitle,
                  child: MyNeumorphicToggle(
                    children: const ['1', '2', '3'],
                    selectedIndex: sharedPreferences.issuerMaxNumberOfLines - 1,
                    onChanged: (newIndex) {
                      sharedPreferences.issuerMaxNumberOfLines = newIndex + 1;
                    },
                  ),
                ),
                secondChild: Container(),
                crossFadeState: sharedPreferences.showIssuer
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: crossFadeDuration,
              ),
              MyFormField(
                label: localizations?.pageSettingsOptionLabelTitle,
                child: MyNeumorphicToggle(
                  children: const ['1', '2', '3'],
                  selectedIndex: sharedPreferences.labelMaxNumberOfLines - 1,
                  onChanged: (newIndex) {
                    sharedPreferences.labelMaxNumberOfLines = newIndex + 1;
                  },
                ),
              ),
              SwitchWithHeadline(
                label:
                    localizations?.pageSettingsOptionShowGlobalProgressBarTitle,
                value: sharedPreferences.showGlobalProgressBar,
                onChanged: (newValue) {
                  sharedPreferences.showGlobalProgressBar = newValue;
                },
              ),
              AnimatedCrossFade(
                firstChild: NeumorphicTextFormField(
                  label: localizations
                      ?.pageSettingsOptionGlobalProgressBarPeriodTitle,
                  keyboardType: TextInputType.number,
                  initialValue:
                      sharedPreferences.globalProgressBarPeriod.toString(),
                  onChanged: (String? value) {
                    int newValue = int.tryParse('$value') ??
                        sharedPreferences.globalProgressBarPeriod;
                    if (newValue > 0) {
                      sharedPreferences.globalProgressBarPeriod = newValue;
                    }
                  },
                ),
                secondChild: Container(),
                crossFadeState: sharedPreferences.showGlobalProgressBar
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: crossFadeDuration,
              ),
              SwitchWithHeadline(
                label: localizations
                    ?.pageSettingsOptionShowIndividualProgressBarTitle,
                value: sharedPreferences.showIndividualProgressBar,
                onChanged: (newValue) {
                  sharedPreferences.showIndividualProgressBar = newValue;
                },
              ),
              AnimatedCrossFade(
                firstChild: NeumorphicTextFormField(
                  label: localizations
                      ?.pageSettingsOptionIndividualProgressBarPeriodTitle,
                  keyboardType: TextInputType.number,
                  initialValue:
                      sharedPreferences.individualProgressBarPeriod.toString(),
                  onChanged: (String? value) {
                    int newValue = int.tryParse('$value') ??
                        sharedPreferences.individualProgressBarPeriod;
                    if (newValue > 0) {
                      sharedPreferences.individualProgressBarPeriod = newValue;
                    }
                  },
                ),
                secondChild: Container(),
                crossFadeState: sharedPreferences.showIndividualProgressBar
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: crossFadeDuration,
              )
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
