import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/pages/home/home.dart';
import 'package:keyring/pages/onboarding/onboarding.dart';
import 'package:keyring/utils/set_navbar.dart';
import 'package:keyring/utils/shared_preferences_model.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);
  static const routeName = '/welcome';

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  _checkOnboardingSeen() async {
    bool? onboardingSeen =
        (await SharedPreferencesModel().getPreferences()).onboardingSeen;
    if (onboardingSeen == null) {
    } else if (onboardingSeen == false) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        OnboardingPage.routeName,
        (route) => false,
      );
    } else if (onboardingSeen == true) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        HomePage.routeName,
        (route) => false,
      );
    }
  }

  @override
  void initState() {
    _checkOnboardingSeen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setNavbar(context);

    return const MyScaffold();
  }
}
