import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_neumorphic_button.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/components/theme_picker.dart';
import 'package:keyring/components/typography.dart';
import 'package:keyring/pages/home/home.dart';
import 'package:keyring/utils/import_backup.dart';
import 'package:keyring/utils/set_navbar.dart';
import 'package:keyring/utils/shared_preferences_model.dart';
import 'package:provider/provider.dart';

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({Key? key}) : super(key: key);
  static const routeName = '/onboarding';

  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  final PageController controller = PageController(initialPage: 0);
  final ScrollController _scrollController = ScrollController();
  int currentPage = 0;

  _setSeenAndGoToHome() {
    Provider.of<SharedPreferencesModel>(context, listen: false).onboardingSeen =
        true;
    Navigator.pushNamedAndRemoveUntil(
      context,
      HomePage.routeName,
      (route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    setNavbar(context);
    AppLocalizations? localizations = AppLocalizations.of(context);

    List<Widget> pages = <Widget>[
      SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: NeumorphicIcon(
                  Icons.vpn_key,
                  size: 56,
                  style: NeumorphicStyle(
                      color: Theme.of(context).textTheme.headline3?.color),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Headline2(localizations?.pageOnboarding1Title),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: BodyText1(
                  localizations?.pageOnboarding1Body,
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
      ),
      SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Icon(
                  Icons.vpn_key,
                  size: 56,
                  color: Theme.of(context).textTheme.headline3?.color,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Headline2(localizations?.pageOnboarding2Title),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 32),
                child: BodyText1(
                  localizations?.pageOnboarding2Body,
                  fontSize: 20,
                ),
              ),
              const ThemePicker(
                label: false,
              )
            ],
          ),
        ),
      ),
      SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Icon(
                  Icons.vpn_key,
                  size: 56,
                  color: Theme.of(context).textTheme.headline3?.color,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Headline2(localizations?.pageOnboarding3Title),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 32),
                child: BodyText1(
                  localizations?.pageOnboarding3Body,
                  fontSize: 20,
                ),
              ),
              MyNeumorphicButton(
                icon: Icons.file_download_rounded,
                text: localizations?.pageSettingsOptionBackupImport,
                onPressed: () async {
                  final result = await importBackup(context);
                  if (result != null && result.isNotEmpty) {
                    _setSeenAndGoToHome();
                  }
                },
                id: 'page_settings_option_backup_import',
              ),
            ],
          ),
        ),
      ),
    ];

    return MyScaffold(
      safeArea: false,
      scrollController: _scrollController,
      body: PageView(
        scrollDirection: Axis.horizontal,
        controller: controller,
        onPageChanged: (int currentIndex) {
          setState(() {
            currentPage = currentIndex;
          });
        },
        children: pages,
      ),
      floatingActionButton: MyNeumorphicIconButton(
        onPressed: () {
          if (currentPage == pages.length - 1) {
            _setSeenAndGoToHome();
          } else {
            controller.nextPage(
                duration: const Duration(milliseconds: 250),
                curve: Curves.easeInOut);
          }
        },
        tooltip: localizations?.pageOnboardingSkip,
        icon: Icons.chevron_right_rounded,
        id: 'page_onboarding_skip',
      ),
    );
  }
}
