import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/entry_form.dart';
import 'package:keyring/components/my_neumorphic_icon_button.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/data/entry_model.dart';
import 'package:provider/provider.dart';

class AddPage extends StatefulWidget {
  const AddPage({Key? key}) : super(key: key);
  static const routeName = '/add';

  @override
  _AddPageState createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<EntryFormState> formWidgetKey = GlobalKey<EntryFormState>();
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      scrollController: _scrollController,
      title: localizations?.pageAddTitle,
      actions: [
        Consumer(
          builder: (context, EntryModel entryModel, child) {
            return MyNeumorphicIconButton(
              tooltip: localizations?.pageAddSaveTooltip,
              icon: Icons.save_rounded,
              onPressed: () async {
                bool? valid = formKey.currentState?.validate();
                if (valid != null && valid) {
                  formKey.currentState?.save();
                  final dynamic value = formWidgetKey.currentState?.entry;
                  final Entry? entry = Entry.tryFromDynamic(value: value);
                  if (entry != null) {
                    await entryModel.insert(entry);
                    Navigator.pop(context, entry);
                  }
                }
              },
              id: 'page_add_save',
            );
          },
        )
      ],
      body: SingleChildScrollView(
        controller: _scrollController,
        child: EntryForm(
          formKey: formKey,
          key: formWidgetKey,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
