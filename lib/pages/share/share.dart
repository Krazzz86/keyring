import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:keyring/components/my_scaffold.dart';
import 'package:keyring/data/entry.dart';
import 'package:keyring/utils/is_mobile.dart';
import 'package:qr_flutter/qr_flutter.dart';

class SharePageArguments {
  SharePageArguments(this.entry);
  final Entry entry;
}

class SharePage extends StatefulWidget {
  const SharePage({Key? key}) : super(key: key);
  static const routeName = '/share';

  @override
  _SharePageState createState() => _SharePageState();
}

class _SharePageState extends State<SharePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final SharePageArguments args =
        ModalRoute.of(context)?.settings.arguments as SharePageArguments;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    AppLocalizations? localizations = AppLocalizations.of(context);

    return MyScaffold(
      title: localizations?.pageShareTitle,
      body: Center(
        child: Neumorphic(
          child: QrImage(
            data: args.entry.toURI(),
            version: QrVersions.auto,
            size: min(
                  MediaQuery.of(context).size.width,
                  MediaQuery.of(context).size.height,
                ) *
                (isMobile() ? 0.9 : 0.75),
            gapless: false,
            padding: const EdgeInsets.all(0),
            foregroundColor: Theme.of(context).textTheme.headline3?.color,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    super.dispose();
  }
}
