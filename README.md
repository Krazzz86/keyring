# Keyring OTP

[<img height="80" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" />
](https://play.google.com/store/apps/details?id=mindless.keyring&utm_source=repository)
[<img height="80" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.svg" />
](https://flathub.org/apps/details/me.orszulak.keyring)
[<img height="80" alt="Try it out in browser" src="https://gitlab.com/AntoniOrs/keyring/-/raw/develop/data/icons/browser-badge.png" />
](https://antoniors.gitlab.io/keyring/)

## Simply beautiful or beautifully simple OTP generator.

Keyring is a second factor authenticator that generates Time-based and HMAC-based One-Time passwords used by most websites or services in lieu of sms or emails.

Features:

- Mostly neumorphic design
- Follows systems preferred theme
- Scanning QR codes
- Sharing with QR codes
- Plain-text backup
- Secrets are encrypted with encryption key stored in the keystore
- Free and open-source (help always welcome 😊)
