# Changelog

## 1.1.1

- Rebuild with Flutter 2.8

## 1.1.0

- Will ask for your rating

## 1.0.1

- Some more analytics

## 1.0.0

- Initial release
