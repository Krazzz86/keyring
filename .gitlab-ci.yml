variables:
  CI_REPOSITORY_URL: https://$GIT_ACCESS_USER:$GIT_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
  VERSION_CODE: $CI_PIPELINE_IID
  VERSION_NAME: 'cat ./pubspec.yaml | grep version: | sed "s/version: //g"'
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/keyring/"

stages:
  - analyze
  - test
  - build
  - changelog
  - upload
  - tag
  - release

.flutter:
  image: cirrusci/flutter:stable
  before_script:
    - flutter pub get
    - echo "$google_services_json" | base64 --decode > ./android/app/google-services.json
  after_script:
    - rm -f ./android/app/google-services.json || true
    - rm -f ./android/android-signing-keystore.jks || true

.fastlane:
  image: fastlanetools/fastlane
  before_script:
    - gem install bundler

analyze:
  extends: .flutter
  stage: analyze
  script:
    - flutter analyze
  except:
    - tags

test:
  extends: .flutter
  stage: test
  dependencies:
    - analyze
  except:
    - tags
  script:
    - flutter test --coverage
    - lcov --list coverage/lcov.info
    - genhtml coverage/lcov.info --output=coverage
  artifacts:
    expire_in: 1 month
    paths:
      - coverage

build-bundle:
  extends: .flutter
  stage: build
  script:
    - echo "$signing_jks_file" | base64 --decode > ./android/android-signing-keystore.jks
    - flutter packages pub run flutter_launcher_icons:main
    - flutter build appbundle --build-name=$(eval $VERSION_NAME)-$VERSION_CODE --build-number=$VERSION_CODE --obfuscate --split-debug-info=./build/app/outputs/mapping/release
    - cd build/app/intermediates/merged_native_libs/release/out/lib/
    - zip ../../../../../../../build/app/outputs/native-debug-symbols.zip arm64-v8a/* armeabi-v7a/* x86_64/*
  artifacts:
    expire_in: 1 week
    paths:
      - "./build/app/outputs/bundle/release"
      - "./build/app/outputs/mapping/release"
      - "./build/app/outputs/native-debug-symbols.zip"
  dependencies:
    - test
  except:
    - tags

build-apk:
  extends: .flutter
  stage: build
  script:
    - echo "$signing_jks_file" | base64 --decode > ./android/android-signing-keystore.jks
    - flutter packages pub run flutter_launcher_icons:main
    - flutter build apk --build-name=$(eval $VERSION_NAME)-$VERSION_CODE --build-number=$VERSION_CODE
  artifacts:
    expire_in: 1 week
    paths:
      - "./build/app/outputs/apk/release/app-release.apk"
  dependencies:
    - test
  except:
    - tags

build-linux:
  extends: .flutter
  stage: build
  script:
    - sudo apt update -y
    - sudo apt install curl clang cmake ninja-build pkg-config libgtk-3-dev libblkid-dev liblzma-dev libjsoncpp-dev cmake-data libjsoncpp1 libsecret-1-dev libsecret-1-0 librhash0 -y
    - flutter config --enable-linux-desktop
    - flutter build linux
    - cp -r ./data/* build/linux/x64/release/bundle/data/
  artifacts:
    expire_in: 1 week
    paths:
      - "./build/linux/x64/release/bundle/*"
  except:
    - tags

build-web:
  extends: .flutter
  stage: build
  script:
    - flutter build web --base-href="/keyring/"
  artifacts:
    expire_in: 1 week
    paths:
      - "./build/web"
  dependencies:
    - test
  except:
    - tags

changelog:
  extends: .fastlane
  stage: changelog
  script:
    - cd android
    - bundle install
    - bundle exec fastlane changelog version_code:$VERSION_CODE
  artifacts:
    expire_in: 1 week
    paths:
      - "./build/"
      - "./android/fastlane/metadata/android/en-GB/changelogs"
  dependencies:
    - build-bundle
    - build-apk
    - build-web
  except:
    - tags

upload-internal:
  extends: .fastlane
  stage: upload
  dependencies:
    - changelog
  script:
    - cd android
    - bundle install
    - bundle exec fastlane upload track:internal
  except:
    - /^release/.*/
    - main
    - develop
    - tags

upload-alpha:
  extends: .fastlane
  stage: upload
  dependencies:
    - changelog
  script:
    - cd android
    - bundle install
    - bundle exec fastlane upload track:alpha
  only:
    - develop
  except:
    - /^release/.*/
    - tags

upload-beta:
  extends: .fastlane
  stage: upload
  dependencies:
    - changelog
  script:
    - cd android
    - bundle install
    - bundle exec fastlane upload track:beta
  only:
    - main
    - /^release/.*/
  except:
    - tags

upload-firebase:
  stage: upload
  image: node:latest
  dependencies:
    - changelog
  script:
    - npm_config_yes=true npx firebase-tools --non-interactive --token $FIREBASE_TOKEN appdistribution:distribute --app $APP_ID --groups $GROUPS ./build/app/outputs/apk/release/app-release.apk --release-notes-file "./android/fastlane/metadata/android/en-GB/changelogs/$VERSION_CODE.txt"
  except:
    - tags

pages:
  stage: upload
  needs: ["build-web"]
  script:
    - rm -rf public
    - cp -r ./build/web public
  artifacts:
    paths:
      - "./build/"
      - public
  only:
    - main
  except:
    - tags

# promote-beta-to-production:
#   extends: .fastlane
#   stage: upload
#   script:
#     - cd android
#     - bundle install
#     - CURRENT_VERSION=$( bundle exec fastlane version_code track:beta | grep "Found " | grep -o "'[0-9]*'" | grep -o "[0-9]*" )
#     - bundle exec fastlane promote track:beta track_promote_to:production version_code:$CURRENT_VERSION
#   only:
#     - main
#   except:
#     - tags

tag-build:
  stage: tag
  dependencies:
    - changelog
  except:
    - tags
  script:
    - TAG_MESSAGE=$( cat ./android/fastlane/metadata/android/en-GB/changelogs/$VERSION_CODE.txt )
    - ./scripts/push_tag.sh $VERSION_CODE ${GITLAB_USER_NAME} ${GITLAB_USER_EMAIL} $CI_REPOSITORY_URL "$TAG_MESSAGE"

upload-android:
  image: curlimages/curl:latest
  stage: upload
  script:
    - VERSION=$(eval $VERSION_NAME)-$VERSION_CODE
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./build/app/outputs/apk/release/app-release.apk "${PACKAGE_REGISTRY_URL}$VERSION/keyring-android.apk?select=package_file"'
  artifacts:
    paths:
      - "./build/"
      - public
  only:
    - main
  except:
    - tags

upload-linux-x86:
  image: curlimages/curl:latest
  stage: upload
  script:
    - tar czf package.tar.gz -C build/linux/x64/release/bundle/ .
    - VERSION=$(eval $VERSION_NAME)-$VERSION_CODE
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file package.tar.gz "${PACKAGE_REGISTRY_URL}$VERSION/keyring-linux-x86.tar.gz?select=package_file"'
  artifacts:
    paths:
      - "./build/"
      - public
  only:
    - main
  except:
    - tags

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  only:
    - main
  script:
    - VERSION=$(eval $VERSION_NAME)-$VERSION_CODE
    - |
      release-cli create --name "Release $VERSION" --tag-name "release/$VERSION" \
       --assets-link "{\"name\":\"keyring-linux-x86.tar.gz\",\"url\":\"${PACKAGE_REGISTRY_URL}$VERSION/keyring-linux-x86.tar.gz\"}" \
       --assets-link "{\"name\":\"keyring-android.apk\",\"url\":\"${PACKAGE_REGISTRY_URL}$VERSION/keyring-android.apk\"}"
  except:
    - tags
